# TEL19A-Inf2

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/reinerh%2Fdhbw%2Ftel19a-inf2-jupyter/master?urlpath=lab)

Folien, Beispiele und Aufgaben zur Vorlesung "Informatik 2" im Kurs TEL19A an der DHBW Mannheim.

## Links

- [Browser für Vorlesungsmaterial](https://gitlab.com/reinerh/dhbw/tel19a-inf2/-/jobs/artifacts/master/browse/?job=deploy:package)
- [Alles als ZIP zum Herunterladen](https://gitlab.com/reinerh/dhbw/tel19a-inf2/-/jobs/artifacts/master/download?job=deploy:package)
- [Jupyter-Umgebung zum Kurs](https://mybinder.org/v2/gl/reinerh%2Fdhbw%2Ftel19a-inf2-jupyter/master?urlpath=lab)

