/*** AUFGABE: Zerlegen von Listen anhand von Kriterien. ***/

/*** BESCHREIBUNG:
 * Bei diesen Aufgaben sollen Sie jeweils die Funktion std::partition()
 * benutzen, um eine Liste nach bestimmten Kriterien umzusortieren.
 *
 * Alternativ kann ggf. auch std::stable_partition benutzt werden,
 * falls sich die relative Reihenfolge der Elemente ansonsten
 * nicht ändern soll.
 *
 * Hinweis: Für die Verwendung von std::partition muss man ein Lambda
 * definieren, wie in den ersten beiden Laboren zu Sortierverfahren
 * gezeigt. In den Tests werden auch Lambdas definiert, diese können
 * als Kopiervorlage benutzt werden.
 *
 * Eine weitere gute Übung wäre auch, eine ähnliche Funktion wie
 * std::partition() selbst zu schreiben.
 *
 * Musterlösungen siehe ganz unten in dieser Datei.
 ***/

/*** INCLUDES: ***/
#include "catch.hpp" // Fuer die Tests
#include <algorithm>
#include <vector>
using namespace std;

/*** AUFGABENSTELLUNG:
 * Schreiben Sie eine Funktion, die einen vector<int> erwartet und ihn
 * so umsortiert, dass alle geraden Elemente links stehen und alle
 * ungeraden rechts.
 *
 * Schreiben Sie eine Funktion, die einen vector<int> erwartet und ihn
 * so umsortiert, dass alle Elemente links stehen, die kleiner sind
 * als das gegebene x.
 *
 * Hinweis:
 * Fangen Sie an, indem Sie die vorhandenen Tests zum Laufen bringen.
 * Anschließend sollten Sie weitere Tests für andere Werte von x
 * und andere Vektoren hinzufügen.
 ***/
void even_left(vector<int> & v);
void smaller_left(vector<int> & v, int x);


/*** TESTFAELLE: ***/
TEST_CASE("even_left()")
{
    vector<int> v1{3,6,1,17,2,4,8,9,5};
    even_left(v1);

    // Wir suchen die Position des ersten ungeraden Elements:
    auto iter = find_if(v1.begin(), v1.end(), [](int x) { return x % 2 == 1; });
    REQUIRE(iter != v1.end()); // Es muss ein ungerades Element vorhanden sein.

    // Der Bereich links von iter darf nur gerade Zahlen enthalten,
    // der Bereich rechts nur ungerade Zahlen.
    // Um das zu prüfen, benutzen wir die Funktion std::all_of().
    REQUIRE(all_of(v1.begin(), iter, [](int x){ return x % 2 == 0; }));
    REQUIRE(all_of(iter, v1.end(), [](int x){ return x % 2 == 1; }));
}

TEST_CASE("smaller_left()")
{
    vector<int> v1{3,6,2,4,8,9,1};
    smaller_left(v1, 4);

    // Wir suchen die Position der 4:
    auto iter = find(v1.begin(), v1.end(), 4);
    REQUIRE(iter != v1.end());

    // Der Bereich links von iter darf keine Zahl >= 4 enthalten,
    // der Bereich rechts davon keine Zahl < 4.
    // Um das zu prüfen, benutzen wir die Funktion std::all_of().
    REQUIRE(all_of(v1.begin(), iter, [](int x){ return x < 4; }));
    REQUIRE(all_of(iter, v1.end(), [](int x){ return x >= 4; }));
}

/*** LOESUNG: ***/

void even_left(vector<int> & v)
{
    //...
}

void smaller_left(vector<int> & v, int x)
{
    //...
}

/// Für Hinweise oder die Lösung weiter herunterscrollen...












































































































































/*** MUSTERLÖSUNG: ***/
/*
void even_left(vector<int> & v)
{
    // Prädikat zum Testen, ob eine Zahl gerade ist.
    // Dieses Prädikat wird von partition() benutzt.
    auto pred = [](int x) { return x % 2 == 0; };

    partition(v.begin(), v.end(), pred);
}

void smaller_left(vector<int> & v, int x)
{
    // Prädikat zum Testen, ob eine Zahl < x ist.
    // Dieses Prädikat wird von partition() benutzt.
    auto pred = [x](int y) { return y < x; };

    // Anmerkung: Das x in den eckigen Klammern wird von unserer
    //            Funktion "eingefangen", d.h. es kann innerhalb
    //            verwendet werden, obwohl es außerhalb definiert ist.

    partition(v.begin(), v.end(), pred);
}
 */