/*
  Dies ist der Code zu Hashing aus dem Labor vom 24.06.
  Unten stehen einige Folgeaufgaben, die als Erweiterung
  gemacht werden können.
*/

#include <iostream>
#include <string>
#include <vector>
using namespace std;


// Bildet einen String auf eine Zahl ab.
// - möglichst eindeutig
long long to_int(string);

// Bestimmt die Position, an der der 
// String s in v eingefügt werden kann.
int get_insert_pos(
  string s,
  vector<pair<string,string>> v);

// Fügt das Paar (de,en) in v ein.
void insert(
  vector<pair<string, string>> & v,
  string de,
  string en);

// Liefert das englische Wort aus v zum gegebenen
// deutschen Wort.
string get(
  vector<pair<string, string>> & v,
  string de);

int main() {
  
	string test = "Auto";
	cout<<to_int(test) << endl;

  vector<pair<string,string>> v1(7,{"",""});

  insert(v1, "Auto", "car");
  insert(v1, "Hallo", "hello");
  insert(v1, "Welt", "world");


  for (auto el : v1)
  {
    cout << "de: " << el.first
         << ", en: " << el.second << endl;
  }

  cout << "Englisch für \"Auto\": " << get(v1, "Auto") << endl;

  return 0;
}

long long to_int(string test)
{
  long long z = 0;
	for (int i = 0; i < test.length(); i++)
	{
		z = z * 1000 + test[i];
	}
  return z;
}

int get_insert_pos(
  string s,
  vector<pair<string,string>> v)
{
  return to_int(s) % v.size();
}

void insert(
  vector<pair<string, string>> & v,
  string de,
  string en)
{
  int pos = get_insert_pos(de, v);
  v[pos] = { de, en };
}

string get(
  vector<pair<string, string>> & v,
  string de)
{
  int pos = get_insert_pos(de, v);
  return v[pos].second;
}

/* Folgeaufgaben:
  
  Bei Kollisionen sondieren
     - einfache Variante: Nach rechts gehen, bis
       eine freie Position gefunden wurde (beim Einfügen) oder bis das gesuchte Wort gefunden wurde (bei get()).
     - besser: Doppelhashing, Kuckucks-Hashing o.Ä.
 */
