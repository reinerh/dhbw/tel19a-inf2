#ifndef ELEMENT_H
#define ELEMENT_H

#include <sstream>
#include <string>

/// Element-Struktur für Telefonbucheinträge
struct Element
{
  std::string name;     // Wird als Suchschlüssel für den Baum genutzt.
  std::string vorname;
  std::string tel;
  
  // Teilbäume, sind standardmäßig leer.
  Element * links = nullptr;
  Element * rechts = nullptr;
  
  /// Liefert eine String-Repräsentation des Elements.
  std::string str();
  
  /// Liefert true zurück, falls der Knoten ein leerer (also Dummy-) Knoten ist.
  bool empty();
  
  /// Schreibt den angegebenen Namen ins Namensfeld.
  /// Falls der aktuelle Knoten leer ist, werden Kind-Elemente erzeugt.
  void setName(std::string name_);
  
  /// Schreibt den angegebenen Vornamen in das aktuelle Element.
  /// Falls der aktuelle Knoten leer ist, passiert nichts.
  void setVorname(std::string vorname);
  
  /// Schreibt die angegebene Telefonnummer in das aktuelle Element.
  /// Falls der aktuelle Knoten leer ist, passiert nichts.
  void setTel(std::string nummer);
  
  /// Macht diesen Knoten zum Dummy
  void clear();
  
  /// Liefert die Anzahl der Elemente in diesem Teilbaum.
  int size();
  
  // Funktionen, die in den Aufgaben implementiert werden müssen.
  int count(char); // Aufgabe 1
};

/* Implementierung der Member von Element.
 *
 * Erinnerung:
 * Aus Gründen der Compilereffizienz und zur einfacheren Wiederverwendung
 * gehören diese Teile eigentlich in eine eigene .cpp-Datei.
 * Zwecks einfacherer Projektverwaltung bleibt dieser Code aber hier im Header.
 */

std::string Element::str()
{
  std::ostringstream s;
  
  if (!vorname.empty())
  {
    s << vorname << " ";
  }
  s << name;
  if (!tel.empty())
  {
    s << ", Tel: " << tel;
  }
  
  return s.str();
}

bool Element::empty()
{
  return links == nullptr || rechts == nullptr;
}

void Element::setName(std::string name_)
{
  if (empty())
  {
    name = name_;
    links = new Element();
    rechts = new Element();
  }
}

void Element::setVorname(std::string vorname_)
{
  if (!empty())
  {
    vorname = vorname_;
  }
}

void Element::setTel(std::string nummer_)
{
  if (!empty())
  {
    tel = nummer_;
  }
}

void Element::clear()
{
  if (!empty())
  {
    delete links;
    links = nullptr;
    delete rechts;
    rechts = nullptr;
  }
}

int Element::size()
{
  if (empty()) { return 0; }
  return 1 + links->size() + rechts->size();
}

#endif