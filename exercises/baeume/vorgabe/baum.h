#ifndef BAUM_H
#define BAUM_H

#include "element.h"

#include <string>

class Baum
{

public:
  /// Konstruktor
  Baum();
  
  /// Fügt ein Element hinzu.
  void insert(std::string name);
  
  /// Liefert einen Pointer auf das Element mit dem angegebenen Namen.
  Element * find(std::string name);
  
  /// Liefert true zurück, falls der Baum leer ist.
  bool empty();
  
  /// Liefert die Anzahl der Elemente im Baum zurück.
  int size();   
  
  // Funktionen, die in den Aufgaben implementiert werden müssen.
  int count(char);                 // Aufgabe 1
  void insert_unique(std::string); // Aufgabe 2
  std::string vn_smallest();       // Aufgabe 3
  
private:
  Element * root;
};

/* Implementierung der Member von Baum.
 *
 * Erinnerung:
 * Aus Gründen der Compilereffizienz und zur einfacheren Wiederverwendung
 * gehören diese Teile eigentlich in eine eigene .cpp-Datei.
 * Zwecks einfacherer Projektverwaltung bleibt dieser Code aber hier im Header.
 */

Baum::Baum()
: root(new Element())
{}

void Baum::insert(std::string name_)
{
  Element * current = root;
  while (!current->empty())
  {
    current = (current->name < name_) ? current->links : current->rechts;
  }
  current->setName(name_);
}

Element * Baum::find(std::string name_)
{
  Element * current = root;
  while (!current->empty() && current->name != name_)
  {
    current = (current->name < name_) ? current->links : current->rechts;
  }
  return current;
}

bool Baum::empty()
{
  return root->empty();
}

int Baum::size()
{
  return root->size();
}

#endif