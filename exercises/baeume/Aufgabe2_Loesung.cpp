
/*** INCLUDES: ***/
#include "vorgabe/pruefung.h"
using namespace std;


/*** AUFGABENSTELLUNG:
Erweitern Sie die folgende Funktion insert_unique(), so dass sie
ein neues Element nur dann in den Baum einfügt, wenn noch kein Element
mit diesem Namen vorhanden ist.
Ist schon ein solches Element vorhanden, soll nichts passieren.

Hinweis: Die vorgegebene Funktion ist identisch mit Baum::insert().
Sie muessen die Implementierung anpassen, um die Aufgabe zu erfüllen.
***/

/*** LOESUNG: ***/
void Baum::insert_unique(string name_)
{
  Element * current = root;
  while (!current->empty())
  {
    if (current->name == name_) { return; }
    current = (current->name < name_) ? current->links : current->rechts;
  }
  current->setName(name_);
}

/*** TESTCODE/MAIN: ***/
int main()
{
  // Testfall 1: In einen leeren Baum können mehrere verschiedene Elemente einfgefügt werden.)
  Baum b1;
  b1.insert_unique("Name1");
  b1.insert_unique("Name2");
  b1.insert_unique("Name3");
  
  cout << b1.size() << endl; // Soll 3 ausgeben


  // Testfall 2: Der gleiche Name kann nur einmal einfgefügt werden.
  
  Baum b2;
  b2.insert_unique("Name1");
  b2.insert_unique("Name1");
  
  cout << b2.size() << endl; // Soll 1 ausgeben
  
  return 0;
}
