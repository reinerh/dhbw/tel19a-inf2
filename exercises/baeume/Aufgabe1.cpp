
/*** INCLUDES: ***/
#include "vorgabe/pruefung.h"
using namespace std;


/*** AUFGABENSTELLUNG:
Implementieren Sie die Funktion Element::count().
Die Funktion soll die Anzahl der Elemente im jeweiligen Teilbaum liefern,
die mit dem angegebenen Buchstaben anfangen.
***/

/*** LOESUNG: ***/
int Element::count(char c)
{
  return 0;
}

/*** TESTCODE/MAIN: ***/
int main()
{
  // Testfall 1: Bei einem leeren Baum liefert count() immer 0.
  Baum b1;
  
  cout << b1.count('a') << endl; // Soll 0 ausgeben
  cout << b1.count('z') << endl; // Soll 0 ausgeben
  cout << b1.count('A') << endl; // Soll 0 ausgeben
  cout << b1.count('K') << endl; // Soll 0 ausgeben

  // Testfall 2: Drei Elemente
  Baum b2;
  b2.insert("aab");
  b2.insert("bab");
  b2.insert("aac");
  
  cout << b2.count('a') << endl; // Soll 2 ausgeben
  cout << b2.count('b') << endl; // Soll 1 ausgeben
  cout << b2.count('c') << endl; // Soll 0 ausgeben
  
  return 0;
}