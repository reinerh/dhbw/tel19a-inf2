
/*** INCLUDES: ***/
#include "vorgabe/pruefung.h"
using namespace std;


/*** AUFGABENSTELLUNG:
Implementieren Sie die Funktion Baum::vn_smallest().
Die Funktion soll zum Element mit dem lexikographisch kleinsten
Namen den Vornamen liefern.
Falls der Baum leer ist, soll ein leerer String geliefert werden.
***/

/*** LOESUNG: ***/
string Baum::vn_smallest()
{
  return "";
}

/*** TESTCODE/MAIN: ***/
int main()
{
  // Testfall 1: Bei einem leeren Baum liefert vn_smallest() den leeren String.
  Baum b1;
  cout << b1.vn_smallest() << endl; // Soll den leeren String ausgeben.
	
  // Testfall 2: "5 Elemente"
  
  // Baum mit 5 Elementen erzeugen.
  Baum b2;
  b2.insert("aaa");
  b2.insert("aa");
  b2.insert("ab");
  b2.insert("cb");
  b2.insert("ca");
  
  // Kleinsten Wert "erkennbar" machen.
  b2.find("aa")->setVorname("bb");
	  
  cout << b2.vn_smallest() << endl; // Soll 'bb' ausgeben
  
  return 0;
}
