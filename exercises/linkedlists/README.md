# Übungsaufgaben zu einfach verketteten Listen

In diesem Verzeichnis gibt es eine Reihe von Aufgaben zu verketteten Listen.

## Überblick über die Vorgabe:

Da die bereitgestellte Klasse mehrere Funktionen enthält, ist die Implementierung auf mehrere Dateien verteilt.

Es sind vier Dateien vorgegeben: `linkedlist.h`, `linkedlist.cpp`, `test.cpp` und `aufgaben.cpp`.
In den beiden ersten ist eine Implementierung von einfach verketteten Listen vorgegeben,
mit der Sie weiterarbeiten sollen.

- Der Header `linkedlist.h` enthält die Definition der Datentypen, die hier verwendet werden.
- Die Quelldatei `linkedlist.cpp` enthält die Implementierung der vorgegebenen
  Funktionen aus `linkedlist`. Nicht alle Funktionen der Liste sind schon implementiert.
- `test.cpp` bindet den Header `linkedlist.h` ein und benutzt/prüft die Funktionen der Liste.
- `aufgaben.cpp` enthält Aufgabenstellungen und Code-Vorgaben für die restlichen
  Funktionen aus `linkedlist.h`. Dies sind die zu bearbeitenden Aufgaben.
  In `test.cpp` werden auch diese Funktionen bereits getestet.
  Ihre Aufgabe ist also, diese Tests zum Laufen zu bringen.
