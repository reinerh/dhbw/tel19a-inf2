# Übungsaufgaben

In diesem Verzeichnis gibt es eine Reihe von Aufgaben zu den verschiedenen Vorlesungsthemen.

## Bauen:

Einige der Aufgaben bestehen aus mehreren Dateien.
Es müssen dann jeweils auch mehrere `.cpp`.Dateien zusammen compiliert werden.
Hier sind einige Hinweise, wie das gemacht werden kann, am Beispiel der Aufgaben zu einfach verketteten Listen (Ordner `linkedlists`).

### Direkt mittels der Konsole:

Um ein Projekt zu übersetzen, das aus mehreren Dateien besteht, müssen Sie beim
Compileraufruf alle Quelldateien (aber nicht die Header) angeben, die mit übersetzt
werden sollen. In diesem Fall wäre die Zeile für den Aufruf:

```g++ -o linkedlist linkedlist.cpp test.cpp```

Dieser Aufruf übersetzt die Dateien`linkedlist.cpp` und `test.cpp` und erzeugt dabei
die ausführbare Datei `linkedlist.exe` (bzw. nur `linkedlist` unter Linux und MacOS).

Falls Sie das Tool *CMake* installiert haben, können Sie auf der Konsole auch einfach
folgendes eingeben (wenn Sie im Projektverzeichnis sind):

```cmake --build .```

Zuvor und ggf. nach jeder Änderung an der Datei `cmakelists.txt` (s.u.) muss einmal `cmake .` im Projektverzeichnis ausgeführt werden.

CMake ist ein Tool, das Abhängigkeiten und notwendige Dateien erkennt und den Bau eines
Projekts auslösen kann. Es ist Open Source und daher kostenlos verfügbar.
Wenn Sie z.B. Visual Studio oder CLion installiert haben, ist die Chance hoch, dass Sie
es schon haben.

### Als Projekt in Entwicklungsumgebungen:

Viele Entwicklungsumgebungen können direkt mit CMake-Projekten umgehen,
z.B. *Visual Studio*, *CLion* oder *QtCreator*.
Sie sollten dort einfach diesen Code-Ordner als Projekt laden können und startklar sein.
Alternativ können Sie in Entwicklungsumgebungen eigene Projekte anlegen und die benötigten
Quell- und Header-Dateien hinzufügen.

### Anmerkung zu CMake:

Das Projekt wird über die Datei `cmakelists.txt` konfiguriert.
Wenn Sie weitere Quelldateien hinzufügen, müssen Sie die Zeile `add_executable(...)` anpassen.
Sie können auch mehrere solche Zeilen hinzufügen, damit definieren Sie dann verschiedene
sog. *Build-Targets*. D.h. verschiedene Executables und welche Quellen dazugehören.

Auch Visual Studio Code ist mittels CMake zumindest etwas leichter einzurichten.
Sie brauchen dafür separat einen Compiler (z.B. MinGW), CMake und natürlich Visual Studio Code
und dessen C++-Plugin. Als Build-Befehl für ein Projekt wählt man dann einfach den obigen
Build-Befehl `cmake --build .`. Es ist aber immer noch deutlich komplizierter als
Visual Studio oder CLion.
