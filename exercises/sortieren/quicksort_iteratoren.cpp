/*** AUFGABE: Implementierung von QuickSort ***/

/*** INCLUDES: ***/
#include "catch.hpp" // Fuer die Tests
#include <algorithm>
#include <vector>
#include<iostream>
using namespace std;

/*** AUFGABENSTELLUNG:
 *   Schreiben Sie eine Funktion QuickSort.
 *   Die Funktion soll einen Vektor aus Zahlen als Referenz erwarten und diesen sortieren.
 *
 *   Bei dieser Aufgabe soll die Funktion keinen Vektor, sondern stattdessen seine
 *   begin()- und end()-Iteratoren erwarten. Dadurch kann bequem ohne zusätzlichen
 *   Speicherverbrauch in-place sortiert werden.
 *
 *   Dazu kann man die Funktion std::partition benutzen.
 *   Es bietet sich an, die entsprechenden Aufgaben vorher zu lösen.
 */
void quickSort(vector<int>::iterator begin, vector<int>::iterator end);
// Lösung siehe ganz unten in der Datei.

/*** HILFSFUNKTIONEN: ***/
// Wendet bubbleSort() auf den Vektor v1 an und prüft, ob er anschließend gleich v2 ist.
void check_sorted_vectors_equal(vector<int> v1, vector<int> v2)
{
    quickSort(v1.begin(), v1.end());
    REQUIRE( v1 == v2 );
}


/*** TESTFAELLE: ***/
TEST_CASE("Listen werden korrekt sortiert.")
{
    check_sorted_vectors_equal(
            { 3,1,2,5,3,7,9,2 },
            { 1,2,2,3,3,5,7,9 } );

    check_sorted_vectors_equal(
            { 1,3,5,7,9 },
            { 1,3,5,7,9 } );

    check_sorted_vectors_equal(
            { 10,8,5,0,-1 },
            { -1,0,5,8,10 } );
}

/*** LOESUNG: ***/
void quickSort(vector<int>::iterator begin, vector<int>::iterator end)
{
    //...
}

/// Für Hinweise oder die Lösung weiter herunterscrollen...















































































































































/*** VORGABE MIT HINWEISEN: ***/
/*
void quickSort(vector<int>::iterator begin, vector<int>::iterator end)
{
    // Rekursionsabbruch: Bei Länge 0  oder 1 ist nichts zu tun.
    if (end - begin <= 1) { return; }

    // Pivotelement wählen
    int pivot = *begin;

    // Partitionieren des Vektors:
    // Alles, was kleiner ist, als pivot, muss links davon stehen,
    // alles andere rechts davon.

    // Hinweis: Man kann dafür die Funktion std::partition oder std::stable_partition benutzen.

    // Die beiden Teillisten rekursiv sortieren.

    // Anders als bei der anderen QuickSort-Aufgabe muss nichts mehr zusammengesetzt
    // werden. Wir haben ja gar keine Hilfslisten verwendet.
}
*/

// Komplette Lösung noch weiter unten...




















































/*** MUSTERLOESUNG: ***/
/*
void quickSort(vector<int>::iterator begin, vector<int>::iterator end)
{
    // Rekursionsabbruch: Bei Länge 0  oder 1 ist nichts zu tun.
    if (end - begin <= 1) { return; }

    // Pivotelement wählen
    int pivot = *begin;

    // Partitionieren des Vektors:
    // Alles, was kleiner ist, als pivot, muss links davon stehen,
    // alles andere rechts davon.
    auto predicate = [pivot](int x) { return x < pivot; };
    auto pivotpos = partition(begin+1, end, predicate)-1;
    iter_swap(begin, pivotpos);

    // Die beiden Teillisten rekursiv sortieren.
    quickSort(begin, pivotpos);         // Von begin bis ohne Pivotelement
    quickSort(pivotpos+1, end);         // Ab hinter Pivotelement bis zum Ende.

    // Anders als bei der anderen QuickSort-Aufgabe muss nichts mehr zusammengesetzt
    // werden. Wir haben ja gar keine Hilfslisten verwendet.
}

// Anmerkungen zum Partitionieren:
// - std::partition() sortiert den Bereich um, so dass alles, was die
//   Bedingung erfüllt, links steht. Die Funktion gibt aber keine
//   weiteren Garantien bzgl. der Reihenfolge. Insbesondere wissen
//   wir also nicht, wo das Pivotelement anschließend stehen wird.
// - Deshalb partitionieren wir den Bereich *nach* begin und swappen
//   anschließend das Pivotelement an die richtige Stelle.
// - partition() liefert einen Iterator auf den Anfang des zweiten Teils.
//   Deshalb pivotpos = partition()-1.
*/