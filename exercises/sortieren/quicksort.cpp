/*** AUFGABE: Implementierung von QuickSort ***/

/*** INCLUDES: ***/
#include "catch.hpp" // Fuer die Tests
#include <algorithm>
#include <vector>
using namespace std;

/*** AUFGABENSTELLUNG:
 *   Schreiben Sie eine Funktion QuickSort.
 *   Die Funktion soll einen Vektor aus Zahlen als Referenz erwarten und diesen sortieren.
 */
void quickSort(vector<int> & v);
// Lösung siehe ganz unten in der Datei.

/*** HILFSFUNKTIONEN: ***/
// Wendet bubbleSort() auf den Vektor v1 an und prüft, ob er anschließend gleich v2 ist.
void check_sorted_vectors_equal(vector<int> v1, vector<int> v2)
{
    quickSort(v1);
    REQUIRE( v1 == v2 );
}


/*** TESTFAELLE: ***/
TEST_CASE("Listen werden korrekt sortiert.")
{
    check_sorted_vectors_equal(
        { 3,1,2,5,3,7,9,2 },
        { 1,2,2,3,3,5,7,9 } );
    
    check_sorted_vectors_equal(
        { 1,3,5,7,9 },
        { 1,3,5,7,9 } );

    check_sorted_vectors_equal(
        { 10,8,5,0,-1 },
        { -1,0,5,8,10 } );
}

/*** LOESUNG: ***/
void quickSort(vector<int> & v)
{
    //...
}

/// Für Hinweise oder die Lösung weiter herunterscrollen...















































































































































/*** VORGABE MIT HINWEISEN: ***/
/*
void quickSort(vector<int> & v)
{
    // Rekursionsabbruch: Bei Länge 0 oder 1 ist nichts zu tun.
    if (v.size() <= 1) { return; }

    // Pivotelement wählen
    int pivot = v[0];

    // Partitionieren des Vektors:
    // Alles, was kleiner ist, als pivot, muss links davon stehen,
    // alles andere rechts davon.
    vector<int> small;
    vector<int> large;
    // ...

    // Die beiden Teillisten rekursiv sortieren.
    // ...

    // Die sortierten Listen wieder in v zusammensetzen.
    // ...
}
*/

// Komplette Lösung noch weiter unten...




















































/*** MUSTERLOESUNG: ***/
/*
void quickSort(vector<int> & v)
{
    // Rekursionsabbruch: Bei Länge 0 oder 1 ist nichts zu tun.
    if (v.size() <= 1) { return; }

    // Pivotelement wählen
    int pivot = v[0];

    // Partitionieren des Vektors:
    // Alles, was kleiner ist, als pivot, muss links davon stehen,
    // alles andere rechts davon.
    vector<int> small;
    vector<int> large;
    for (int i=1; i<v.size(); i++)
    {
        if (v[i] < pivot)
        {
            small.push_back(v[i]);
        }
        else
        {
            large.push_back(v[i]);
        }
    }

    // Die beiden Teillisten rekursiv sortieren.
    quickSort(small);
    quickSort(large);

    // Die sortierten Listen wieder in v zusammensetzen.
    int j = 0;
    for (int i=0; i<small.size(); i++)
    {
        v[j++] = small[i];
    }
    v[j++] = pivot;
    for (int i=0; i<large.size(); i++)
    {
        v[j++] = large[i];
    }
}
*/