/*** AUFGABE: Implementierung von BubbleSort ***/

/*** INCLUDES: ***/
#include "catch.hpp" // Fuer die Tests
#include <algorithm>
#include <vector>
using namespace std;

/*** AUFGABENSTELLUNG:
 *   Schreiben Sie eine Funktion BubbleSort.
 *   Die Funktion soll einen Vektor aus Zahlen als Referenz erwarten und diesen sortieren.
 */
void bubbleSort(vector<int> & v);
// Musterlösung siehe ganz unten in der Datei.

/*** HILFSFUNKTIONEN: ***/
// Wendet bubbleSort() auf den Vektor v1 an und prüft, ob er anschließend gleich v2 ist.
void check_sorted_vectors_equal(vector<int> v1, vector<int> v2)
{
    bubbleSort(v1);
    REQUIRE( v1 == v2 );
}

/*** TESTFAELLE: ***/
TEST_CASE("Listen werden korrekt sortiert.")
{
    check_sorted_vectors_equal(
        { 3,1,2,5,3,7,9,2 },
        { 1,2,2,3,3,5,7,9 } );
    
    check_sorted_vectors_equal(
        { 1,3,5,7,9 },
        { 1,3,5,7,9 } );

    check_sorted_vectors_equal(
        { 10,8,5,0,-1 },
        { -1,0,5,8,10 } );
}

/*** LOESUNG: ***/
void bubbleSort(vector<int> & v)
{
    /* ... */
}

/// Für die Musterlösung weiter herunterscrollen...





























































































































































/*** MUSTERLOESUNG: ***/
/*
void bubbleSort(vector<int> & v)
{
    for (size_t end=v.size()-1; end>0; end--)
    {
        for (size_t i = 0; i<end; i++)
        {
            if (v[i] > v[i+1]) { swap(v[i], v[i+1]); }
        }
    }
}
*/