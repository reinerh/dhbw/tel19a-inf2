#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include<vector>
#include<algorithm>
#include<ctime>
#include<cstdlib>
using namespace std;

bool sorted(vector<int> const & v)
{
    for (size_t i=0; i<v.size()-1; i++)
    {
        if (v[i] > v[i+1]) { return false; }
    }
    return true;
}

void uglySort(vector<int> & v)
{
    int size = v.size();
    while (!sorted(v))
    {
        int i = rand() % size;
        int j = rand() % size;
        if (i<j && v[i] > v[j])
        {
            swap(v[i], v[j]);
        }
    }
}

TEST_CASE("uglySort()")
{
    srand(time(nullptr));
    
    vector<int> v1{3,1,5,2,6,8,32,42,23,38};
    uglySort(v1);
    REQUIRE(v1 == vector<int>{1,2,3,5,6,8,23,32,38,42});
}
