# Theorieaufgaben zu Sortierverfahren

## Listen sortieren:

### Einfache Sortierverfahren:

Sortieren Sie die folgenden Listen jeweils mit den Verfahren `InsertionSort`, `SelectionSort` und `BubbleSort`.

Geben Sie dabei nach jedem Durchlauf der äußeren Schleifen die Liste an und markieren Sie (bei `InsertionSort` und `SelectionSort`) die Stelle, bis zu der der Algorithmus sicher sein, kann, dass die Liste sortiert ist.

- `1, 13, -2, 3, 2, 7, 42, 38`
- `3, 2, 1, 0`
- `25, 38, 42, 107`

### HeapSort:
- Sortieren Sie die folgende Liste mittels `HeapSort`:

- `3 1 7 2 5 -1 8 42`

Machen Sie dabei deutlich, in welche Teillisten die Liste zerlegt wird und wann diese zu welchen Teillisten zusammengesetzt werden.

### QuickSort:

- Sortieren Sie die folgende Liste mittels `HeapSort`:

- `3 1 7 2 5 -1 8 42`

Geben Sie dabei in jedem Schritt an, welches Pivotelement gewählt wird und wie die vorsortierten Teillisten aussehen.

\newpage

## Sortierverfahren erkennen:

Erklären sie, wie das folgende Sortierverfahren funktioniert:

```c++
void uglySort(vector<int> & v)
{
    int size = v.size();
    while (!sorted(v))
    {
        int i = rand() % size;
        int j = rand() % size;
        if (i<j && v[i] > v[j])
        {
            swap(v[i], v[j]);
        }
    }
}
```

Die Funktion `sorted()` gibt dabei `true` zurück, wenn die Liste sortiert ist.
