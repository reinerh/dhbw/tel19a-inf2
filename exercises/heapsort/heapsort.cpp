/*
 * Dies ist der Beispielcode aus dem Labor vom 23. Juni. Wir haben mehrere Funktionen implementiert:
 * 
 * - heapAdd():  Erwartet einen Vektor und behandelt ihn als Heap.
 *               D.h. der Vektor muss die Struktur eines Heaps aufweisen.
 *               Fügt ein neues Element am Ende ein und lässt es aufsteigen, bis es passt.
 *               Diese Funktion wird zum Sortieren nicht gebraucht, soll aber das Prinzip verdeutlichen.
 *               
 * - bubbleUp(): Hilfsfunktion für heapAdd(), implementiert das Aufsteigen.
 * 
 * - heapSort(): Erwartet einen Vektor und sortiert ihn.
 *               Der Vektor ist am Anfang kein Heap, enthält aber Elemente. Die Hilfsfunktion
 *               bubbleUp() wird verwendet, um nacheinander jedes Element aufsteigen zu lassen.
 *               So wird aus dem Vektor ein Heap.
 *               Die Funktion heapSort() ist noch unvollständig.
 * 
 * - test...():  Testfunktionen, um von der Main aus einfach zwischen Tests für den Heap und
 *               für heapSort() umschalten zu können.
 *
 * Offene Aufgabe:
 * heapSort() wird erst dadurch zum Sortierverfahren, dass am Ende nacheinander jedes Wurzelelement gelöscht wird.
 * Vgl. dazu die Vorlesung zu Heaps. „Löschen“ bedeutet aber in diesem Fall, dass Das Element ans Ende getauscht wird
 * und dass dann die neue Wurzel einsinkt. Dies ist in heapSort() schon vorgegeben, aber die Funktion bubbleDown() fehlt noch,
 * die das Einsinken bewerkstelligt.
 *
 * Weitere Aufgaben zu Heaps:
 * - Schreiben Sie eine Funktion, die prüft, ob ein Vektor die Heap-Struktur hat.
 *   Diese Funktion soll also einen Vektor erwarten und true oder false liefern.
 * - Schreiben Sie eine rekursive Variante von bubbleUp().
 */

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

void heapSort(vector<int> &);
void test_heapSort();

void heapAdd(vector<int> &, int);

void test_heapAdd();

int main()
{
  test_heapAdd();

//  test_heapSort();

  return 0;
}

void test_heapAdd()
{
  vector<int> v1;

  heapAdd(v1, 40);
  heapAdd(v1, 25);
  heapAdd(v1, 3);
  heapAdd(v1, 50);
  heapAdd(v1, 12);

  // Sollte 50 40 3 25 12 ausgeben
  for (auto el : v1)
  {
    cout << el << " ";
  }
  cout << endl;

}

// Hilfsfunktion für heapAdd():
// Lässt das Element von Stelle n aufsteigen, bis es richtig einsortiert ist.
void bubbleUp(vector<int> & v, int n)
{
  while (n > 0 && v[n] > v[(n-1) / 2])
  {
    swap(v[n], v[(n-1) / 2]);
    n = (n-1) / 2;
  }
}

// Fügt ein neues Element zu v als Heap hinzu.
void heapAdd(vector<int> & v, int x)
{
  v.push_back(x);
  bubbleUp(v, v.size()-1);
}

void test_heapSort()
{

  vector<int> v1 { 5, 2, 7, 13, -1, 42, 38 };

  heapSort(v1);

  for (auto el : v1)
  {
    cout << el << " ";
  }
  cout << endl;
}

/// Implementierung von heapSort().

void heapSort(vector<int> & v) {
  // Anfangsbedingung:
  // v ist gefüllt, aber als Heap noch leer
  
  // 1. Schritt: "Heapifizieren"
  //   D.h. wir sortieren die Elemente von v so um,
  //   dass v zu einem Max-Heap wird.

  //   Für jedes Element einmal bubbleUp() aufrufen.
  //   Hinweis: bubbleUp() muss berücksichtigen,
  //   welche Elemente schon zum Heap gehören und
  //   welche noch nicht.
  for (int i=0; i<v.size(); i++)
  {
    bubbleUp(v,i);
  }

  // 2. Schritt: Alle Elemente wieder entfernen
  //   -> auch umsortieren
  for (int i=v.size()-1; i>=0; i--)
  {
    swap(v[0],v[i]); // Wurzel ans Ende tauschen
    bubbleDown(v,i); // Neue Wurzel wieder einsortieren
  }
  
  // Rest-Aufgabe: bubbleDown() implementieren.
  // Das Element an der Wurzel muss dabei bis maximal zur Stelle i einsinken, oder bis sie größer ist, als ihre beiden Kinder.
}