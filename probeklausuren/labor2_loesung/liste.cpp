/*** AUFGABE: Listen ***/
/*** PUNKTE: 5 ***/

/*** INCLUDES: ***/
#include <iostream>
#include <string>
#include "hilfsfunktionen.h"
#include "linkedlist.h"
using namespace std;

/*** AUFGABENSTELLUNG:
In der Datei 'linkedlist.h' ist eine Element-Datenstruktur für eine
einfach verkettete Liste vorgegeben.
Wie üblich soll das letzte Element immer ein Dummy sein, so dass auch eine leere
Liste zumindest ein leeres Element enthält.

Schreiben Sie eine Funktion, die ein solches Element als Pointer erwartet und die prüft,
ob es sich um eine aufsteigend sortierte Liste handelt.
***/

/*** LÖSUNG: ***/
bool isSorted(Element * e)
{
  return e->empty() || e->next->empty() || (e->data < e->next->data && isSorted(e->next));
}

/*** TESTCODE/MAIN: ***/
int main()
{
  // Eine leere Liste erzeugen.
  Element head;
  cout << isSorted(&head) << endl;   // Soll 1 ausgeben (Die leere Liste ist sortiert).
  
  // Ein Element einfügen.
  head.data = 13;
  head.next = new Element();
  cout << isSorted(&head) << endl;   // Soll 1 ausgeben (Eine einelementige Liste ist sortiert).
  
  // Ein weiteres Element anhängen.
  head.next->data = 25;
  head.next->next = new Element();
  cout << isSorted(&head) << endl;   // Soll 1 ausgeben.
  
  // Ein falsches Element anhängen (Sortierung geht kaputt).
  head.next->next->data = 7;
  head.next->next->next = new Element();
  cout << isSorted(&head) << endl;   // Soll 0 ausgeben.
  
  return 0;
}