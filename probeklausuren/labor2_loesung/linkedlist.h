#ifndef LINKEDLIST_H
#define LINKEDLIST_H

struct Element
{
  int data = 0;
  Element * next = nullptr;
  
  bool empty();
};

bool Element::empty()
{
  return next == nullptr;
}


#endif