# Probe-Theorie-Aufgaben

## Sortieren

Betrachten Sie die folgende Liste von Zahlen:

```
3 13 2 25 8 42 
```

Sortieren Sie die folgende Liste von Zahlen aufsteigend mit den verschiedenen einfachen Sortierverfahren aus der Vorlesung:

- *InsertionSort*
- *BubbleSort*
- *SelectionSort*

Geben Sie dabei jeweils die Sortierung nach jedem inneren Durchlauf an.

## Sortieren

Sortieren Sie verschiedene Listen mit den Verfahren *QuickSort* und *MergeSort*:

- Bei QuickSort: Markieren Sie in jedem Schritt das Pivot-Element und geben Sie an, welche Teillisten erzeugt werden.
- Bei MergeSort: Geben Sie schrittweise an, wie die Liste in Einzellisten zerlegt und wieder zusammengesetzt wird.

## Listen

Erläutern Sie die Funktionsweise der folgenden (unvollständigen) Listen-Datenstruktur:

```c++
struct Element
{
    int data;
    Element * next = nullptr;
    Element * next10 = nullptr;
    
    int get(int pos)
    {
        if (pos >= 10 && next10 == nullptr) { return -1; }
        if (pos >= 10) { return next10->get(pos-10); }
        if (pos > 0 && next == nullptr) { return -1; }
        if (pos > 0) { return next->get(pos-1); }
        return data;
    }
}
```
Erläutern Sie Vor- und Nachteile dieser Struktur im Vergleich mit anderen Listen-Datenstrukturen.
Erläutern Sie zusätzlich die Einschränkungen dieser Struktur. D.h. was muss bei der Verwendung (insb. der `get()`-Funktion) beachtet werden?

## Bäume

Betrachten Sie die folgenden Zahlen:

``` 1 3 4 8 13 23 42 ```

Geben Sie für diese Zahlen eine Reihenfolge an, so dass sich beim Einfügen in einen binären Suchbaum ein vollständiger Binärbaum ergibt.

## AVL-Bäume
Fügen Sie die folgenden Zahlen nacheinander in einen AVL-Baum ein. Zeichnen Sie den Baum vor und nach jeder Rotation.

``` 13 25 12 8 10 2 1 ```

## Heaps

Zeichnen Sie die Max-Heaps, die sich jeweils durch Einfügen der folgenden Elemente ergeben:

`3 8 5 17 42 13`\
`8 3 17 5 13 42`\
`10 2 15 4 1 12 28`

## HeapSort

Sortieren Sie die folgende Liste In-Place mittels HeapSort:

`25 38 2 1 12 18 23`

1. Geben Sie die Liste an, wie sie nach dem "Heapifizieren" aussieht. D.h. fügen Sie die Elemente nacheinander in einen Max-Heap ein und geben Sie diesen Heap in Listendarstellung an.
2. Geben Sie dann nach jedem Löschen der Wurzel an, wie die Liste anschließend (nach Wiederherstellung des Heaps) aussieht.