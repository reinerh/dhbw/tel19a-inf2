/*** AUFGABE: Heaps ***/
/*** PUNKTE: 6 ***/

/*** INCLUDES: ***/
#include <iostream>
#include <string>
#include "hilfsfunktionen.h"
#include "heap.h"
using namespace std;

/*** AUFGABENSTELLUNG:
In der Datei 'heap.h' ist eine Heap-Implementierung vorgegeben.
Der Konstruktor übernimmt einen Element-Vektor der bereits alle Elemente enthält.
Die Elemente müssen aber noch nicht die Heap-Eigenschaft haben.
Im Konstruktor wird eine Funktion 'heapify()' aufgerufen, die die Heap-Eigenschaft
herstellen soll.

Implementieren Sie die Funktion Heap::heapify(), so dass die Elemente anschließend
einen Max-Heap ergeben.
***/

/*** LÖSUNG: ***/
void Heap::heapify()
{
  // Bitte schreiben Sie hier Ihre Lösung
}

/*** TESTCODE/MAIN: ***/
int main()
{
  // Einen Vektor erzeugen und ihn an einen Heap weitergeben.
  vector<int> v1 = {3,1,5};
  Heap h1(v1);
  
  // Der Heap sollte bei seiner Konstruktion die Elemente umsortieren, so dass sie einen Max-Heap bilden.
  print(v1);   // Soll "5 1 3" ausgeben
  
  // Weitere Tests mit anderen Vektoren
  vector<int> v2 = {4,2,1,8,13};
  Heap h2(v2);
  print(v2);   // Soll "13 8 1 2 4" ausgeben
  
  // Weitere Tests mit anderen Vektoren
  vector<int> v3 = {3,1,15,42,38};
  Heap h3(v3);
  print(v3);   // Soll "42 38 3 1 15" ausgeben
  
  return 0;
}