#ifndef BINBAUM_H
#define BINBAUM_H

struct Element
{
  int data = 0;
  Element * left = nullptr;
  Element * right = nullptr;
  
  bool empty();
  bool isSearchTree();
};

bool Element::empty()
{
  return left == nullptr && right == nullptr;
}



#endif