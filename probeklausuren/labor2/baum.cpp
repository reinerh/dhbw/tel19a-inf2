/*** AUFGABE: Bäume ***/
/*** PUNKTE: 6 ***/

/*** INCLUDES: ***/
#include <iostream>
#include <string>
#include "hilfsfunktionen.h"
#include "binbaum.h"
using namespace std;

/*** AUFGABENSTELLUNG:
In der Datei 'binbaum.h' ist eine einfache Element-Datenstruktur für einen
Binärbaum vorgegeben.
Neben den üblichen Membern gibt es eine Funktion isSearchTree(), die prüfen soll,
ob ein Element mitsamt seinen Kindern ein binärer Suchbaum ist.

Implementieren Sie die Funktion isSearchTree().
***/

/*** LÖSUNG: ***/
bool Element::isSearchTree()
{
  // Bitte schreiben Sie hier Ihre Lösung
  return true;
}

/*** TESTCODE/MAIN: ***/
int main()
{
  // Einen Baum erzeugen.
  Element root;
  cout << root.isSearchTree() << endl;   // Soll 1 ausgeben (Jeder leere Baum ist ein Suchbaum).
  
  // Root einen Wert geben.
  root.data = 42;
  root.left = new Element();
  root.right = new Element();
  cout << root.isSearchTree() << endl;   // Soll 1 ausgeben
  
  // Ein Kind links von der Wurzel erzeugen, dessen Wert kleiner ist.
  root.left->data = 23;
  root.left->left = new Element();
  root.left->right = new Element();
  cout << root.isSearchTree() << endl;   // Soll 1 ausgeben
  
  // Ein Kind rechts von der Wurzel erzeugen, dessen Wert kleiner ist.
  root.right->data = 15;
  root.right->left = new Element();
  root.right->right = new Element();
  cout << root.isSearchTree() << endl;   // Soll 0 ausgeben
  
  return 0;
}