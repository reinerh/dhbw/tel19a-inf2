#ifndef HILFSFUNKTIONEN_H
#define HILFSFUNKTIONEN_H

#include<vector>
#include<iostream>

template <typename T>
void print(std::vector<T> const & v)
{
  for (auto el : v)
  {
    std::cout << el << " ";
  }
  std::cout << std::endl;
}

#endif