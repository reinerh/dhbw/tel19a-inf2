#ifndef HEAP_H
#define HEAP_H

#include<vector>

struct Heap
{
  // Elemente des Baums
  std::vector<int> & elements;
  
  // Marker für die Größe. Bis vor dieser Position gehören die Elemente zum Baum.
  int current_size;

  // Konstruktor: Übernimmt einen bereits gefüllten Elemente-Vektor.
  // Der konstruierte Heap ist allerdings leer, weil 
  Heap(std::vector<int> & elements_);
  
  // Macht aus dem Elemente-Vektor einen Max-Heap.
  void heapify();
};

// Implementierung des Konstruktors.
// Übernimmt den Element-Vektor und legt die Größe anfänglich auf 0 fest.
// Benutzt anschließend die heapify()-Funktion, um einen Heap herzustellen.
Heap::Heap(std::vector<int> & elements_)
: elements(elements_)
, current_size(0)
{
  heapify();
}


#endif