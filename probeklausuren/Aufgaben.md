# Probe-Theorie-Aufgaben

## Sortieren

Betrachten Sie die folgende Liste von Zahlen:

```23 3 1 42 7 38```

Sortieren Sie die folgende Liste von Zahlen aufsteigend mit den verschiedenen einfachen Sortierverfahren aus der Vorlesung:

- *InsertionSort*
- *BubbleSort*
- *SelectionSort*

Geben Sie dabei jeweils die Sortierung nach jedem inneren Durchlauf an.

*Hinweis:* Mit "innerem Durchlauf" ist jeweils das Sortieren eines Elements an seine Stelle gemeint. D.h. bei InsertionSort die Liste, die nach dem Einsortieren des jeweils nächsten Elements aus dem noch unsortierten Teil vorliegt.

Beispiel für InsertionSort:

`23` `3 1 42 7 38` \
`3 23` `1 42 7 38` \
`1 3 23` `42 7 38` \
`1 3 23 42` `7 38` \
`1 3 7 23 42` `38` \
`1 3 7 23 38 42`

## Sortieren

Sortieren Sie verschiedene Listen mit den Verfahren *QuickSort* und *MergeSort*:

- Bei QuickSort: Markieren Sie in jedem Schritt das Pivot-Element und geben Sie an, welche Teillisten erzeugt werden.
- Bei MergeSort: Geben Sie schrittweise an, wie die Liste in Einzellisten zerlegt und wieder zusammengesetzt wird.

Beispiel QuickSort: \
`15 10 30 8 2 25 42` \
`10 8 2` 15 `30 20 42` \
...

Beispiel MergeSort: \
`15 10 30 8 2 25 42` \
`15 10 30` `8 2 25 42` \
`15` `10 30` `8 2` `25 42` \
`15` `10` `30` `8` `2` `25` `42` \
`15` `10 30` `2 8` `25 42` \
`10 15 30` `2 8 25 42` \
`2 8 10 15 30 25 42`


## Bäume

Geben Sie die binären Suchbäume an, die sich jeweils durch Einfügen der Zahlen ergeben:

`15 3 1 17 28 38 25`\
`3 25 42 1 3 5`\
`5 9 1 2 42 23 15`

#### Zusatzaufgabe:
Fügen Sie die Zahlen jeweils auch in einen AVL-Baum ein. D.h. führen Sie die jeweiligen Rotationen durch und zeichnen Sie den Baum vor und nach jeder Rotation.

## Struktur von Bäumen

Die beiden folgenden Listen geben den selben Binärbaum in Pre-Order- und in In-Order-Darstellung an:

`2 15 4 12 3 8 25`\
`4 15 12 2 8 3 25`

Zeichnen Sie den Baum.

## Heaps

Zeichnen Sie die Min-Heaps, die sich jeweils durch Einfügen der folgenden Elemente ergeben:

`3 8 5 17 42 13`\
`8 3 17 5 13 42`\
`10 2 15 4 1 12 28`

## HeapSort

Sortieren Sie die folgende Liste In-Place mittels HeapSort:

`2 1 7 8 15 24 12`

1. Geben Sie die Liste an, wie sie nach dem "Heapifizieren" aussieht. D.h. fügen Sie die Elemente nacheinander in einen Max-Heap ein und geben Sie diesen Heap in Listendarstellung an.
2. Geben Sie dann nach jedem Löschen der Wurzel an, wie die Liste anschließend (nach Wiederherstellung des Heaps) aussieht.