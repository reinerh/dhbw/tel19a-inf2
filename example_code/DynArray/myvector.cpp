#include "myvector.h"

#include <sstream>
using namespace std;

MyVector::MyVector()
{
  data = new int[5];
  max_size = 5;
  size = 0;
}

void MyVector::push_back(int el)
{
  if (size == max_size)
  {
    int * newdata = new int[2 * max_size];
    for (int i=0; i<size; i++)
    {
      newdata[i] = data[i];
    }
    delete data;
    data = newdata;
    max_size *= 2; // max_size = max_size * 2;
  }
  data[size] = el;
  size += 1;
}

void MyVector::pop_back()
{
  if (size > 0)
  {
    size--;
  }
}

void MyVector::insert(int i, int x)
{
  if (i<0) { return; }
  if (i>=size) { return; }
  push_back(data[size-1]);
  // Alle Elemente um 1 nach rechts kopieren, die zwischen i und der ursprünglichen Größe liegen.
  for (int j=size-1; j>i; j--)
  {
    data[j] = data[j-1];
  }
  data[i] = x;
}

void MyVector::swap(int i, int j)
{
  if (i<0) { return; }
  if (i>=size) { return; }
  if (j<0) { return; }
  if (j>=size) { return; }

  int h = data[i];
  data[i] = data[j];
  data[j] = h;
}

int MyVector::at(int i)
{
  return data[i];
}

int MyVector::operator[](int i)
{
  return at(i);
}

string MyVector::str()
{
  ostringstream s;
  for (int i=0; i<size; i++)
  {
    s << data[i] << " ";
  }
  return s.str();
}

ostream & operator<<(ostream & left, MyVector & right)
{
  left << right.str();
  return left;
}

