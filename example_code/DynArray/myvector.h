#ifndef MYVECTOR_H
#define MYVECTOR_H

#include <string>

class MyVector {

public:
  // Konstruktor (Initialiert ein Element vom Typ MyVector)
  MyVector();

  // Ein Element am Ende hinzufügen
  void push_back(int);

  // Entfernt das letzte Element
  void pop_back();

  // Fügt x an Stelle i ein.
  void insert(int i, int x);

  // Vertauscht die Elemente an den Stellen i und j
  void swap(int i, int j);

  // Liefert das Element an Stelle i
  int at(int i);

  // Subscript-Operator: Liefert das Element an Stelle i
  int operator[](int i);

  // Gibt den Vector als String zurück
  std::string str();

private:
  int * data;    // Pointer auf die eigentlichen Daten (ein Array)
  int size;      // Die aktuelle Größe (die Anzahl der Elemente in den Daten)
  int max_size;  // Die maximale Größe (die Länge von data)
};

std::ostream & operator<<(std::ostream &, MyVector &);

# endif
