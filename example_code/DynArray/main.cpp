#include "myvector.h"

#include <iostream>
using namespace std;

int main() {
  MyVector v1;

  v1.push_back(42);
  v1.push_back(4);
  v1.push_back(2);
  v1.push_back(23);
  v1.push_back(17);

  v1.push_back(38);
  v1.pop_back();
  v1.insert(3, 105);
  v1.swap(0,2);

  cout << v1 << endl;
  cout << v1[3] << endl;

  return 0;
}
