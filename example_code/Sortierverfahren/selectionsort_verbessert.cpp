#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

// Klasse für Datensätze über Fahrzeuge
struct Vehicle
{
  string name;
  int speed; // Höchstgeschwindigkeit in km/h 
  int power; // Leisung in PS 
  int cost;  // Preis in EUR
};

// Deklaration des Operators < für Vehicle,
// dadurch kann der Compiler Fahrzeuge direkt vergleichen.
bool operator<(Vehicle const &, Vehicle const &);

// In der Standardbibliothek gibt es auch:
// bool operator<(int, int);
// bool operator<(float, float);
// bool operator<(string, string);

void print(vector<int> v);
void print(vector<Vehicle> v);

template<typename T>
void selectionSort(vector<T> & v);

int main()
{
  vector<Vehicle> v1
  {
    { "Rennwagen", 300, 500, 200000 },
    { "Gurke", 80, 30, 500 },
    { "Flugzeug", 1100, 10000, 10 },
    { "Yacht", 20, 2000, 500 }
  };
  vector<int> v2 { 3, 1, 7, 2, 5, 17, 42, 23, 107, 38 };

  selectionSort(v1);
  selectionSort(v2);
  
  print(v1);
  print(v2);

  return 0;
}

// Sucht das kleinste Element in v ab der Stelle startPos
// und liefert dessen Position zurück.
template<typename T> // Funktions-Template
int find_smallest(vector<T> const & v, int startPos)
{
  // Position des bisher kleinsten Elements
  int pos = startPos;

  for (int i = startPos; i<v.size(); i++)
  {
    // Wenn das Element an der aktuellen Position kleiner ist als
    // das Element an Stelle pos, dann aktualisieren wir pos.
    if (v[i] < v[pos])
    {
      pos = i;
    }
  }
  return pos;
}

template<typename T>
void selectionSort(vector<T> & v)
{
  // Größe des sortierten Bereichs
  // und gleichzeitig
  // Position des ersten Elements im unsortierten Teil.
  int sortiert = 0;

  // Solange der sortierte Bereich noch nicht die gesamte Liste ist...
  while (sortiert < v.size())
  {
    // ... kleinstes Element um unsortierten Teil suchen ...
    int smallestPos = find_smallest(v, sortiert);

    // ... und an den Anfang des unsortierten Teils tauschen.
    swap(v[smallestPos], v[sortiert]);

    // Sortiert-Marker weiterschieben
    sortiert++;
  }
}

bool operator<(Vehicle const & left, Vehicle const & right)
{
  return left.cost < right.cost;
}

void print(vector<Vehicle> v)
{
  for (auto el:v)
  {
    cout << el.name << ": " << el.speed << ", " << el.power << ", " << el.cost << endl;
  }
}

void print(vector<int> v)
{
  for (auto el:v)
  {
    cout << el << " ";
  }
  cout << endl;
}