#include <iostream>
#include <vector>
#include <string>
using namespace std;

// Datensatz-Struktur für zu sortierende Personen.
struct Person
{
  string name;
  int age;
};

// Operator für den Vergleich zweier Personen
bool operator<(Person const & left, Person const & right);

// Sortiert die Elemente in v mittels Insertion Sort
template <typename T>
void insertionSort(vector<T> & v);

int main() {
  
vector<Person> v1{
  { "Max Mustermann", 42 },
  { "Monika Musterfrau", 23 },
  { "Arno Dübel", 38 }
};

  insertionSort(v1);

  for (auto const & el:v1)
  {
    cout << el.name << ": " << el.age << endl;
  }
  return 0;
}

// Lässt das Element an Stelle pos nach links in v einsinken,
// bis es größer ist als sein linker Nachbar oder bis es an Stelle 0 steht.

// Wenn der Teil links von pos aufsteigend sortiert ist, dann wird das neue Element
// dadurch richtig einsortiert.
template <typename T>
void bubbleLeft(vector<T> & v, int pos)
{
  while (pos > 0 && v[pos] < v[pos-1])
  {
    swap(v[pos], v[pos-1]);
    pos--;
  }
}

template <typename T>
void insertionSort(vector<T> & v)
{
  // Mitzählen, wie viele Elemente schon sortiert sind
  int sortiert = 0;

  // Solange die Liste noch nicht ganz sortiert ist, ...
  while (sortiert < v.size())
  {
    // ... das jeweils erste Element aus dem unsortierten Teil nach links einsortieren.
    bubbleLeft(v, sortiert);
    sortiert++;
  }
}

bool operator<(Person const & left, Person const & right)
{
  return left.name < right.name;
}
