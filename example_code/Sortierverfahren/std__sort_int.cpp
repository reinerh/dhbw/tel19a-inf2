#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
  vector<int> v1{3,2,5,7,4};

  // Vergleichsfunktion für Zahlen
  // "Komparator", Stichwort "anonyme Funktionen", "Lambdas"
  auto comp = [](int i1, int i2)
  {
    return i1 < i2;
  };

  sort(v1.begin(), v1.end(), comp);

  for (auto el:v1)
  {
    cout << el << " ";
  }
  cout << endl;
}