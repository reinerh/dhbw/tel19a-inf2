#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

void print(vector<int> v);
void selectionSort(vector<int> & v);

int main() {

  vector<int> v1{ 5, 1, 8, -3, 42, 107, 2, -3, 13, 38 };
  vector<int> v2{ 1,3,5,7,9 };
  vector<int> v3{ 13, 11, 5, 2, -3 };
  vector<int> v4;

  selectionSort(v1);
  selectionSort(v2);
  selectionSort(v3);
  selectionSort(v4);

  print(v1);
  print(v2);
  print(v3);
  print(v4);

  return 0;
}

// Sucht das kleinste Element in v ab der Stelle startPos
// und liefert dessen Position zurück.
int find_smallest(vector<int> const & v, int startPos)
{
  // Position des bisher kleinsten Elements
  int pos = startPos;

  for (int i = startPos; i<v.size(); i++)
  {
    // Wenn das Element an der aktuellen Position kleiner ist als
    // das Element an Stelle pos, dann aktualisieren wir pos.
    if (v[i] <= v[pos])
    {
      pos = i;
    }
  }
  return pos;
}

void selectionSort(vector<int> & v)
{
  // Größe des sortierten Bereichs
  // und gleichzeitig
  // Position des ersten Elements im unsortierten Teil.
  int sortiert = 0;

  // Solange der sortierte Bereich noch nicht die gesamte Liste ist...
  while (sortiert < v.size())
  {
    // ... kleinstes Element um unsortierten Teil suchen ...
    int smallestPos = find_smallest(v, sortiert);

    // ... und an den Anfang des unsortierten Teils tauschen.
    swap(v[smallestPos], v[sortiert]);

    // Sortiert-Marker weiterschieben
    sortiert++;
  }
}






void print(vector<int> v)
{
  for (auto el:v)
  {
    cout << el << " ";
  }
  cout << endl;
}