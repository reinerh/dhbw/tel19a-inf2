# Beispiele zu Sortierverfahren

Dieser Ordner enthält Beispiel-Implementierungen zu Sortierverfahren, wie sie in Vorlesung/Laboren besprochen wurden. Der Unterordner "InsertionSort" enthält zusätzlich weitere Varianten von InsertionSort.
