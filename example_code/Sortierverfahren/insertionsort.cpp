#include <iostream>
#include <vector>
using namespace std;

// Sortiert die Elemente in v mittels Insertion Sort
void insertionSort(vector<int> & v);

int main() {
  
//  vector<int> v1{ 3, 1, 62, 32, 15, 7, -3, 42, 38 };
//  vector<int> v1{ 1, 2, 3, 4, 7, 9, 38 };
// vector<int> v1{ 103, 74, 59, 42, 38 };
vector<int> v1;

  insertionSort(v1);

  for (auto el:v1)
  {
    cout << el << " ";
  }
  cout << endl;

  return 0;
}

// Lässt das Element an Stelle pos nach links in v einsinken,
// bis es größer ist als sein linker Nachbar oder bis es an Stelle 0 steht.

// Wenn der Teil links von pos aufsteigend sortiert ist, dann wird das neue Element
// dadurch richtig einsortiert.
void bubbleLeft(vector<int> & v, int pos)
{
  while (pos > 0 && v[pos] < v[pos-1])
  {
    int h = v[pos-1];
    v[pos-1] = v[pos];
    v[pos] = h;
    pos--;
  }
}

void insertionSort(vector<int> & v)
{
  // Mitzählen, wie viele Elemente schon sortiert sind
  int sortiert = 0;

  // Solange die Liste noch nicht ganz sortiert ist, ...
  while (sortiert < v.size())
  {
    // ... das jeweils erste Element aus dem unsortierten Teil nach links einsortieren.
    bubbleLeft(v, sortiert);
    sortiert++;
  }
}
