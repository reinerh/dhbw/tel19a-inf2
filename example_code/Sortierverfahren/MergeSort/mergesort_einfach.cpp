#include <iostream>
#include <vector>
using namespace std;

void print(vector<int> v);
void mergesort(vector<int> & v);

int main() {
  
  vector<int> v{ 5,2,-1,13,42,38,23,107,86 };

  mergesort(v);

  print(v);

 // std::cout << "Hello World!\n";

  return 0;
}

void mergesort(vector<int> & v)
{
  // Eine Liste der Länge 0 oder 1 ist auf natürliche Weise sortiert.
  // Wir haben dann nichts zu tun und brechen ab.
  if (v.size() <= 1) { return; }

  // Zerlege v in zwei Teile (möglichst gleich groß).
  vector<int> links;
  vector<int> rechts;
  int mitte = v.size()/2;

  // Alles bis zur Mitte nach links kopieren.
  for (int i=0; i<mitte; i++)
  {
    links.push_back(v[i]);
  }
  // Alles ab der Mitte nach rechts kopieren.
  for (int i=mitte; i<v.size(); i++)
  {
    rechts.push_back(v[i]);
  }

  // Sortiere die beiden Teile.
  mergesort(links);
  mergesort(rechts);

  // Mische die sortierten Teile zu einem sortierten Vektor zusammen (in v).
  int pos_links = 0;
  int pos_rechts = 0;
  int pos_v = 0;

  // 1. Solange wir bei beiden Listen noch nicht am Ende sind..
  while (pos_links < mitte && pos_rechts < v.size() - mitte)
  {
    // Kopiere das kleinere Element aus den beiden Teillisten in v
    // und erhöhe den entsprechenden Zähler.
    if (links[pos_links] < rechts[pos_rechts])
    {
      v[pos_v] = links[pos_links];
      pos_links++;
    }
    else
    {
      v[pos_v] = rechts[pos_rechts];
      pos_rechts++;
    }
    pos_v++;
  }

  // 2. Kopiere die restlichen Elemente von Links.
  while (pos_links < mitte)
  {
    v[pos_v] = links[pos_links];
    pos_links++;
    pos_v++;
  }

  // 3. Kopiere die restlichen Elemente von Rechts.
  while (pos_rechts < v.size() - mitte)
  {
    v[pos_v] = rechts[pos_rechts];
    pos_rechts++;
    pos_v++;
  }
}

void print(vector<int> v)
{
  for (auto el:v)
  {
    cout << el << " ";
  }
  cout << endl;
}
