#ifndef MERGESORT_H
#define MERGESORT_H

#include <vector>

void mergesort(std::vector<int> & v);

#endif