#include "catch.hpp"

#include <iostream>
#include <vector>
using namespace std;

#include "mergesort.h"


TEST_CASE( "MergeSort: Zufälliger Vektor" )
{
  vector<int> v{ 5,2,-1,13,42,38,23,107,86 };
  mergesort(v);

  REQUIRE( v == vector<int> { -1, 2, 5, 13, 23, 38, 42, 86, 107 } );
}

TEST_CASE( "MergeSort: Leerer Vektor" )
{
  vector<int> v;
  mergesort(v);

  REQUIRE( v == vector<int> {} );
}

TEST_CASE( "MergeSort: Umgekehrt sortierter Vektor" )
{
  vector<int> v{77, 32, 23, 12, 8, 0, -10};
  mergesort(v);

  REQUIRE( v == vector<int> {-10, 0, 8, 12, 23, 32, 77} );
}



