#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

// Das gleiche Beispiel-Struct wie beim verbesserten InsertionSort
struct Person
{
  string name;
  int age;
};
bool operator<(Person const & left, Person const & right);

int main() {

  // Wir benutzen zwei Vektoren unterschiedlichen Typs
  vector<int> v1 { 3, 2, 5, 17, 1, 42, 38 };
  vector<Person> v2{
    { "Max Mustermann", 42 },
    { "Monika Musterfrau", 23 },
    { "Arno Dübel", 38 }
  };

  // Sortiert die Vektoren mittels des Operators <
  sort(v1.begin(), v1.end());
//  sort(v2.begin(), v2.end());

  // Sortiert v2 ohne Operator <
  // mittels der Funktion comp, die hier als erstes definiert wird.
  // "comp" steht für "Komparator".
  // comp gibt an, ob das Element left links von right einsortiert werden soll oder nicht.
  auto comp = [](Person const & left, Person const & right)
  {
    //return left.name < right.name;
    return right < left;
  };
  sort(v2.begin(), v2.end(), comp);

  // Mehr Infos zum Erzeugen von Funktionen wie oben:
  // Stichwort "Lambdas" oder "Anonyme Funktionen"

  for (auto const & el : v1)
  {
    cout << el << " ";
  }
  cout << endl;

  for (auto const & el:v2)
  {
    cout << el.name << ": " << el.age << endl;
  }
  cout << endl;

  return 0;
}

bool operator<(Person const & left, Person const & right)
{
  return left.age < right.age;
}

