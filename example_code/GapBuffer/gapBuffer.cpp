#include <iostream>
#include <vector>
#include <string>
#include <sstream>
using namespace std;

class GapBuffer
{
public:
  void insert(char c);
  void move(int offset);
  
  string str();
  void printInfo();

  // Weitere denkbare Funktionen (Übungsaufgabe)

  // Ganze Strings hinzufügen
  void insert(string s);

  // Eines oder mehrere Zeichen löschen
  void erase();

private:
  void grow(int count);

  int gapBegin = 0;
  int gapEnd = 0;
  vector<char> data;
};

int main() {
  
  GapBuffer b1;

  b1.insert('H');
  b1.insert('a');
  b1.insert('l');
  b1.insert('o');

  b1.move(-1);
  b1.insert('l');

  b1.printInfo();

  return 0;
}

void GapBuffer::insert(char c)
{
  if (gapBegin == gapEnd)
  {
    grow(10);
  }

  data[gapBegin] = c;
  gapBegin++;
}

void GapBuffer::grow(int count)
{
  // Platz für count neue Elemente schaffen
  for (int i=0; i<count; i++)
  {
    data.push_back(' ');
  }

  // Verbesserung (Übungsaufgabe):
  // std::rotate
  // Alles rechts von der Lücke verschieben.
  for (int i=data.size()-count-1; i>=gapEnd; i--)
  {
    data[i+count] = data[i];
  }
  gapEnd += count;
}


string GapBuffer::str()
{
  ostringstream s;

  // Alles vor der Lücke nach s ausgeben.
  for (int i=0; i<gapBegin; i++)
  {
    s << data[i];
  }

  s << '#';

  // Alles nach der Lücke nach s ausgeben.
  for (int i=gapEnd; i<data.size(); i++)
  {
    s << data[i];
  }

  return s.str();
}

void GapBuffer::move(int offset)
{
  int gapLength = gapEnd - gapBegin;
  if (offset < 0)
  {
    // Lücke nach links verschieben
    // also Elemente von links nach rechts kopieren
    for (int i=gapBegin-1; i>=gapBegin+offset; i--)
    {
      data[i+gapLength] = data[i];
    }
  }
  else
  {
    // Lücke nach rechts verschieben
    // also Elemente nach links kopieren
    for (int i=gapEnd; i<gapEnd+offset; i++)
    {
      data[i-gapLength] = data[i];
    }
  }
  gapBegin += offset;
  gapEnd += offset;
}

void GapBuffer::printInfo()
{
  cout << str() << endl;
  cout << '|';
  for (auto el:data)
  {
    cout << el;
  }
  cout << '|' << endl;
  cout << "gapBegin: " << gapBegin << endl;
  cout << "gapEnd: " << gapEnd << endl;
  cout << "data.size: " << data.size() << endl;
}


