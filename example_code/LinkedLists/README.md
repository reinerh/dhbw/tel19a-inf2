# Beispiel zu einfach verketteten Listen

Da die bereitgestellte Klasse mehrere Funktionen enthält, ist die Implementierung auf mehrere Dateien verteilt.

Es sind drei Dateien vorhanden: `linkedlist.h`, `linkedlist.cpp` und `test.cpp`.
Die beiden ersten enthalten eine Implementierung von einfach verketteten Listen,
`test.cpp` enthält eine `main`-Funktion sowie einige Hilfsfunktionen, die die
Funktionalität der Liste prüfen.
