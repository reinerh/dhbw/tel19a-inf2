/** Beschreibung dieser Quelldatei:
 *
 * Diese Datei enthält die Implementierungen für die Funktionen,
 * die in linkedlist.h als Member-Funktionen vom LinkedList deklariert sind.
 */

// Benötigte Includes
#include"linkedlist.h"

#include<iostream>
#include<string>
#include<sstream>

Element::Element()
: data(0)
, next(nullptr)
{}

bool Element::isDummy()
{
    return (next == nullptr);
}

LinkedList::LinkedList()
: head(new Element())
{}

void LinkedList::push_back(int x)
{
    // Zeiger, der am Anfang auf den Kopf der Liste zeigt.
    Element * current = head;
    
    // Zeiger verschieben, bis der Dummy (das Element nach dem Ende) gefunden ist.
    while (!current->isDummy())
    {
        current = current->next;
    }
    
    // Daten des Dummys setzen und einen neuen Dummy anhängen.
    current->data = x;
    current->next = new Element();
}

void LinkedList::insert(int pos, int x)
{
    // Zeiger, der am Anfang auf den Kopf der Liste zeigt.
    Element * current = head;

    // Sonderfall: Wenn pos == 0 ist, muss head ersetzt werden.
    if (pos == 0)
    {
        Element * el = new Element();
        el->data = x;
        el->next = head;
        head = el;
        return;
    }
    
    // Zeiger verschieben, bis er auf den Vorgänger des pos-ten Elements zeigt.
    // Wenn die Liste kürzer ist, abbrechen.
    while (!current->isDummy() && pos > 1)
    {
        current = current->next;
        pos--;
    }
    if (current->isDummy() && pos != 0) { return; }
    
    // Neues Element erzeugen und zwischen current und current->next einhängen.
    Element * el = new Element();
    el->data = x;
    el->next = current->next;
    current->next = el;
}

std::string LinkedList::str() const
{
    // Zeiger, der am Anfang auf den Kopf der Liste zeigt.
    Element * current = head;
    
    // Stringstream als Ausgabepuffer
    std::stringstream s;
    
    // Durch die Liste laufen und die Daten jedes Elements an s anhängen.
    while(!current->isDummy())
    {
        s << current->data;
        if (!current->next->isDummy()) { s << " "; }
        current = current->next;
    }
    
    // Den in s gepufferten String zurückgeben
    // Anmerkung: Das hier ist keine Rekursion!
    // Es ist der Aufruf einer Memberfunktion str() von StringStream.
    return s.str();
}

std::ostream & operator<<(std::ostream & left, LinkedList const & right)
{
    return left << right.str();
}

