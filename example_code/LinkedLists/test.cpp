/** Main-Funktion und Tests für die Klasse LinkedList.
 *
 * Wir definieren hier eine Reihe von Testfunktionen für die Liste.
 * Jede der Testfunktionen definiert eine oder mehrere Beispiellisten
 * und ruft dann Funktionen darauf auf, um zu prüfen, ob alles funktioniert.
 *
 * Wir achten dabeidarauf , dass in jeder Funktion wirklich
 * nur die zu testende Funktion benutzt wird.
 */

#include"linkedlist.h"

#include <iostream>
#include <initializer_list>
using namespace std;

// Vorausdeklaration der Testfunktionen, Implementierung s.u.
void test_LinkedList_push_back();
void test_LinkedList_insert();
void test_LinkedList_insert_front();

int main()
{
    test_LinkedList_push_back();
    test_LinkedList_insert();
    test_LinkedList_insert_front();

    return 0;
}

// Hilfsfunktion: Erzeugt eine Liste aus den angegebenen Zahlen.
LinkedList createList(initializer_list<int> inits)
{
    LinkedList result;
    Element * last = result.head;

    for (auto el : inits)
    {
        last->data = el;
        last->next = new Element();
        last = last->next;
    }

    return result;
}

void test_LinkedList_push_back()
{
    LinkedList l;

    cout << l << endl;                 // Soll "" ausgeben.

    // Füge drei Elemente hinzu
    l.push_back(13);
    l.push_back(42);
    l.push_back(38);

    cout << l << endl;                 // Soll "13 42 38" ausgeben.
}

void test_LinkedList_insert()
{
    LinkedList l = createList({1,3,5});

    // Füge Element mittendrin hinzu
    l.insert(1, 23);

    cout << l << endl;                 // Soll "1 23 3 5" ausgeben.
}

void test_LinkedList_insert_front()
{
    LinkedList l = createList({1,3,5});

    // Füge Element am Anfang hinzu
    l.insert(0, 23);

    cout << l << endl;                 // Soll "23 1 3 5" ausgeben.
}