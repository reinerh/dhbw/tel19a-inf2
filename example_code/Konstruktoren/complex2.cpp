#include <iostream>
#include <utility>
#include <cmath>
using namespace std;

struct Complex
{
  float angle;
  float abs;

public:
  // Standardkonstruktor
  // keine Argumente, soll einen Standardwert liefern.
  Complex();

  // Konstruktor, der Real- und Imaginärteil erwartet
  Complex(float real_, float img_);

  // Kopierkonstruktor
  Complex(Complex const &);

  pair<float,float> cartesian();
  pair<float,float> polar();
};

int main() {
  
  Complex c1{2.3, 4.2};
  Complex c2(c1);
  
  auto cartesian1 = c1.cartesian();
  cout << cartesian1.first << ", " << cartesian1.second << endl;

  auto polar1 = c1.polar();
  cout << polar1.first << ", " << polar1.second << endl;

  return 0;
}

// Implementierung des Standardkonstruktors
Complex::Complex()  // Kopf
: angle{0}, abs{0}   // member initializer list
{}                  // Funktionsrumpf

// Implementierung des "normalen" Konstruktors
Complex::Complex(float real_, float img_)
: angle{atan(img_/real_)}, abs{sqrt(real_*real_ + img_*img_)}
{}

Complex::Complex(Complex const & other)
: angle{other.angle}, abs{other.abs}
{}

pair<float,float> Complex::cartesian()
{
  float img = sin(angle) * abs;
  float real = img / tan(angle);
  return {real, img};
}

pair<float,float> Complex::polar()
{
  return {abs, angle};
}