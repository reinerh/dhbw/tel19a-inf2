#include <iostream>
#include <utility>
#include <cmath>
using namespace std;

struct Complex
{
  float real;
  float img;

public:
  // Standardkonstruktor
  // keine Argumente, soll einen Standardwert liefern.
  Complex();

  // Konstruktor, der Real- und Imaginärteil erwartet
  Complex(float real_, float img_);

  pair<float,float> cartesian();
  pair<float,float> polar();
};

int main() {
  
  Complex c1{2.3, 4.2};

  cout << c1.real << " + " << c1.img << "j" << endl;
  
  auto cartesian1 = c1.cartesian();
  cout << cartesian1.first << ", " << cartesian1.second << endl;

  auto polar1 = c1.polar();
  cout << polar1.first << ", " << polar1.second << endl;

  return 0;
}



// einfache, unoptimierte Variante des Standardkonstruktors
/*
Complex::Complex()
{
  real = 0;
  img = 0;
}
*/

// bessere Version des Standardkonstruktors
Complex::Complex()  // Kopf
: real{0}, img{0}   // member initializer list
{}                  // Funktionsrumpf

// Implementierung des "normalen" Konstruktors
Complex::Complex(float real_, float img_)
: real{real_}, img{img_}
{}

pair<float,float> Complex::cartesian()
{
  return {real, img};
}

pair<float,float> Complex::polar()
{
  float abs = sqrt(real*real + img*img);
  float alpha = atan(img/real);
  return {abs, alpha};
}