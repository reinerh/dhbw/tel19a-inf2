#include "circularbuffer.h"

#include <iostream>
using namespace std;

int main() {
  
  CircularBuffer c1;

  c1.push(42);
  c1.push(28);
  c1.push(25);
  c1.push(10);
  c1.push(107);
  c1.push(1071);
  c1.push(1);

  c1.pop();
  c1.pop();
  c1.pop();
  c1.pop();
  c1.pop();

  c1.push(17);
  c1.push(107);
  c1.push(1);
  c1.push(107);
  c1.push(135);
  c1.push(43);
  c1.push(44);
  c1.push(38);  // Darf nicht mehr ausgeführt werden, weil die Länge dann 10 wäre.

  cout << c1.str() << endl;
  cout << "size: " << c1.size() << endl;

  c1.pop();
  cout << c1.str() << endl;
  cout << "size: " << c1.size() << endl;

  return 0;
}
