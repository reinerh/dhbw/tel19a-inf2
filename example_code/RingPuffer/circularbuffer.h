
#ifndef CIRCULARBUFFER_H
#define CIRCULARBUFFER_H

#include <string>

class CircularBuffer
{
public:
  // Fügt ein Element hinzu.
  void push(int el);

  // Entfernt ein Element
  void pop();

  // Liefert die Anzahl der Elemente
  int size();

  // Gibt den Puffer auf der Konsole aus
  // (Zu Testzwecken)
  std::string str();

private:
  int elements[10];
  int begin = 0;
  int end = 0;
};

#endif

/* Anmerkung zur Größe des Element-Arrays:
 *
 * Wir reservieren hier Platz für 10 Elemente. Tatsächlich ist die Maximalgröße des Ringpuffers dadurch
 * aber 9 (und nicht 10). Der Grund dafür ist, dass sich die begin- und end-Marker ja übereinander bewegen können.
 * Wenn sie gleich sind, ist der Puffer leer. Wenn das 10. Element hinzukommt, sind sie aber wieder gleich.
 * 
 * Wir dürfen daher nur 9 Elemente zulassen. Dann können wir einen vollen Puffer (in push_back()) daran erkennen,
 * dass der end-Marker direkt vor begin steht (und nicht gleich ist).
 */