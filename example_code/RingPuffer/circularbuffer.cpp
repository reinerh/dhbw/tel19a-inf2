#include "circularbuffer.h"

#include <sstream>
using namespace std;

// Fügt ein Element hinzu.
void CircularBuffer::push(int el)
{
  if (end != (begin-1))
  {
    elements[end] = el;
    end++;
    end = end % 10;  // if (end == 10) { end = 0; }
  }
}

// Entfernt ein Element
void CircularBuffer::pop()
{
  if (end != begin)     // if (size() == 0)
  {
    begin++;
  }
}

int CircularBuffer::size()
{
  if (begin > end)
  {
    return 10 + end - begin;
  }
  return end - begin;
}

// Gibt den Puffer auf der Konsole aus
// (Zu Testzwecken)
string CircularBuffer::str()
{
  ostringstream s;
  
  if (begin > end)
  {
    // Gibt die Elemente bis zum Ende des Arrays aus.
    for (int i=begin; i<10; i++)
    {
      s << elements[i] << " ";
    }

    // Gibt die Elemente ab dem Anfang bis Position end aus.
    for (int i=0; i<end; i++)
    {
      s << elements[i] << " ";
    }
  }
  else
  {
    for (int i = begin; i < end; i++)
    {
      s << elements[i] << " ";
    }
  }
  return s.str();
}
