// Wir definieren eine Element-Datenstruktur
// und eine Klasse für den Binärbaum.

#include <iostream>
using namespace std;

struct Element
{
  int data = 0;
  
  Element * links = nullptr;
  Element * rechts = nullptr;

  // Gibt den Teilbaum aus (als sortierte Liste), der zu diesem Element gehört.
  void print(int depth = 0);

  // Fügt ein neues Element in den Teilbaum ein.
  void insert(int d);

  // Sucht das Element mit dem Schlüssel d und gibt dessen Datensatz zurück.
  int find(int d);
};

struct BinTree
{
private:
  Element * root;

public:
  // Konstruktor: Erzeugt einen Baum in initialisiert die Wurzel
  // (mit einem leeren Knoten).
  BinTree();

  // Gibt den Baum als sortierte Liste aus.
  void print();

  // Fügt ein neues Element in den Baum ein.
  void insert(int d);

  // Sucht das Element mit dem Schlüssel d und gibt dessen Datensatz zurück.
  // Anmerkung: In diesem Beispiel sind Schlüssel und Datensatz identisch,
  //            das wird nicht auf Dauer so bleiben.
  int find(int d);
};

int main() {
  
  // Leeren Baum erzeugen:
  BinTree b1;

  // Ein Element "15" einfügen:
  b1.insert(15);

  // Ein Element "30" einfügen:
  b1.insert(30);

  b1.insert(7);
  b1.insert(1);
  b1.insert(10);

//  b1.print();
  cout << endl;


  b1.insert(42);
  b1.insert(35);
  b1.insert(107);
  b1.print();
//  cout << endl;

  /*
  15
   |
   +-7
   | |
   | + 1
   | +10
   |
   + 30
     |
     + 42
       |
       + 35
       + 107
  */


//  cout << b1.find(42) << endl;

}

void Element::print(int depth)
{
  // Pre-Order-Ausgabe mit Einrückungen entsprechend der Tiefe im Baum.
  // --> Ergibt eine lesbare Baumdarstellung (siehe Beispiel in main).
  if (links == nullptr && rechts == nullptr) { return; }

  for (int i=0; i<depth; i++) { cout << " "; }
  cout << data << endl;
  links->print(depth+1); // Steht für (*links).print();
  rechts->print(depth+1);
}

void Element::insert(int d)
{
  // Wenn der aktuelle Knoten leer ist, neues Element erzeugen.
  if (links == nullptr && rechts == nullptr)
  {
    data = d;
    links = new Element();
    rechts = new Element();
    return;
  }

  // Knoten ist nicht leer.
  // Je nach Wert von d nach links oder rechts weitergehen.
  if (d < data)
  {
    links->insert(d);
  }
  else
  {
    rechts->insert(d);
  }
}

int Element::find(int d)
{
  // Wenn der aktuelle Knoten leer ist, haben wir nichts gefunden.
  // TODO

  // Wenn die Daten des aktuellen Knotens d entsprechen, haben das Element gefunden.
  if (d == data) { return data; }

  // Links oder rechts weitersuchen, je nach Wert von d.
  if (d < data)
  {
    return links->find(d);
  }
  else
  {
    return rechts->find(d);
  }
}

BinTree::BinTree()
: root(new Element())
{}

void BinTree::print()
{
  root->print();
}

void BinTree::insert(int d)
{
  root->insert(d);
}

int BinTree::find(int d)
{
  return root->find(d);
}

