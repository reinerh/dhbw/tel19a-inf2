
// Wir definieren eine Element-Datenstruktur
// und eine Klasse für den Binärbaum.
#include <string>
#include <iostream>
using namespace std;

template<typename KEY_TYPE, typename VALUE_TYPE>
struct Element
{
  KEY_TYPE key;
  VALUE_TYPE value;
  
  Element * links = nullptr;
  Element * rechts = nullptr;

  // Gibt den Teilbaum aus (als sortierte Liste), der zu diesem Element gehört.
  void print(int depth = 0);

  // Fügt ein neues Element in den Teilbaum ein.
  void insert(KEY_TYPE key, VALUE_TYPE value);

  // Sucht das Element mit dem Schlüssel d und gibt dessen Datensatz zurück.
  VALUE_TYPE find(KEY_TYPE key);
};

template<typename KEY_TYPE, typename VALUE_TYPE>
struct BinTree
{
private:
  Element<KEY_TYPE, VALUE_TYPE> * root;

public:
  // Konstruktor: Erzeugt einen Baum in initialisiert die Wurzel
  // (mit einem leeren Knoten).
  BinTree();

  // Gibt den Baum als sortierte Liste aus.
  void print();

  // Fügt ein neues Element in den Baum ein.
  void insert(KEY_TYPE key, VALUE_TYPE value);

  // Sucht das Element mit dem Schlüssel d und gibt dessen Datensatz zurück.
  // Anmerkung: In diesem Beispiel sind Schlüssel und Datensatz identisch,
  //            das wird nicht auf Dauer so bleiben.
  VALUE_TYPE find(KEY_TYPE key);
};

int main() {
  
  // Leeren Baum erzeugen:
  BinTree<string, string> b1;

  b1.insert("Hallo", "Hello");
  b1.insert("Welt", "World");
  b1.insert("Baum", "Tree");

  b1.print();

  cout << b1.find("Baum") << endl; // Soll "Tree" ausgeben.


  BinTree<string, int> b2;

  b2.insert("Hallo", 17432);
  b2.insert("Welt", -13);

  b2.print();

}

template<typename KEY_TYPE, typename VALUE_TYPE>
void Element<KEY_TYPE, VALUE_TYPE>::print(int depth)
{
  // Pre-Order-Ausgabe mit Einrückungen entsprechend der Tiefe im Baum.
  // --> Ergibt eine lesbare Baumdarstellung (siehe Beispiel in main).
  if (links == nullptr && rechts == nullptr) { return; }

  for (int i=0; i<2*depth; i++) { cout << " "; }
  cout << key << " : " << value << endl;
  links->print(depth+1); // Steht für (*links).print();
  rechts->print(depth+1);
}

template<typename KEY_TYPE, typename VALUE_TYPE>
void Element<KEY_TYPE, VALUE_TYPE>::insert(KEY_TYPE key_, VALUE_TYPE value_)
{
  // Wenn der aktuelle Knoten leer ist, neues Element erzeugen.
  if (links == nullptr && rechts == nullptr)
  {
    key = key_;
    value = value_;
    links = new Element();
    rechts = new Element();
    return;
  }

  // Knoten ist nicht leer.
  // Je nach Wert von d nach links oder rechts weitergehen.
  if (key_ < key)
  {
    links->insert(key_, value_);
  }
  else
  {
    rechts->insert(key_, value_);
  }
}

template<typename KEY_TYPE, typename VALUE_TYPE>
VALUE_TYPE Element<KEY_TYPE, VALUE_TYPE>::find(KEY_TYPE key_)
{
  // Wenn der aktuelle Knoten leer ist, haben wir nichts gefunden.
  if (links == nullptr && rechts == nullptr)
  {
    // TODO: Es bleibt unklar, was im Fehlerfall eine sinnvolle Rückgabe wäre.
    return VALUE_TYPE{};
  }

  // Wenn die Daten des aktuellen Knotens d entsprechen, haben das Element gefunden.
  if (key_ == key) { return value; }

  // Links oder rechts weitersuchen, je nach Wert von d.
  if (key_ < key)
  {
    return links->find(key_);
  }
  else
  {
    return rechts->find(key_);
  }
}

template<typename KEY_TYPE, typename VALUE_TYPE>
BinTree<KEY_TYPE, VALUE_TYPE>::BinTree()
: root(new Element<KEY_TYPE, VALUE_TYPE>())
{}

template<typename KEY_TYPE, typename VALUE_TYPE>
void BinTree<KEY_TYPE, VALUE_TYPE>::print()
{
  root->print();
}

template<typename KEY_TYPE, typename VALUE_TYPE>
void BinTree<KEY_TYPE, VALUE_TYPE>::insert(KEY_TYPE key, VALUE_TYPE value)
{
  root->insert(key, value);
}

template<typename KEY_TYPE, typename VALUE_TYPE>
VALUE_TYPE BinTree<KEY_TYPE, VALUE_TYPE>::find(KEY_TYPE d)
{
  return root->find(d);
}
