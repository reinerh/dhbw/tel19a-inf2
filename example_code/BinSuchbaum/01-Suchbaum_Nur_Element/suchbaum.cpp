
// Wir definieren eine Element-Datenstruktur
// und bauen von Hand einen Baum damit auf.

#include <iostream>
using namespace std;
struct Element
{
  int data = 0;
  Element * links = nullptr;
  Element * rechts = nullptr;
};

int main() {
  
  // Leeren Baum erzeugen:
  Element * root = new Element();
  // Der Knoten root ist ein Dummy-Element, das wir nicht zum Inhalt des Baumes zählen.

  // Ein Element "15" einfügen:
  root->data = 15;
  root->links = new Element();
  root->rechts = new Element();
  // Jetzt ist root kein Dummy mehr, aber seine Kinder sind Dummys.

  // Ein Element "30"
  root->rechts->data = 30;
  root->rechts->links = new Element();
  root->rechts->rechts = new Element();
  // Das rechte Kind von root wurde in ein "echtes" Element verwandelt.
}
