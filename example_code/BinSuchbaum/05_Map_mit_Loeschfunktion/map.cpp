// Wir definieren eine Element-Datenstruktur
// und eine Klasse für den Binärbaum.
#include <string>
#include <iostream>
using namespace std;

template<typename KEY_TYPE, typename VALUE_TYPE>
struct Element
{
  KEY_TYPE key;
  VALUE_TYPE value;
  
  Element * links = nullptr;
  Element * rechts = nullptr;

  // Liefert true zurück, wenn das Element ein leerer Baum ist (also ein Dummy).
  bool empty();

  // Gibt den Teilbaum aus (als sortierte Liste), der zu diesem Element gehört.
  void print(int depth = 0);

  // Fügt ein neues Element in den Teilbaum ein.
  void insert(KEY_TYPE key, VALUE_TYPE value);

  // Sucht das Element mit dem Schlüssel d und gibt dessen Datensatz zurück.
  VALUE_TYPE find(KEY_TYPE key);

  // Entfernt das erste Element mit dem angegebenen Schlüssel.
  void erase(KEY_TYPE key);

  // Liefert einen Pointer auf das Element mit dem angegeben Schlüssel.
  Element * get_ptr(KEY_TYPE key);
};

template<typename KEY_TYPE, typename VALUE_TYPE>
struct BinTree
{
private:
  Element<KEY_TYPE, VALUE_TYPE> * root;

public:
  // Konstruktor: Erzeugt einen Baum und initialisiert die Wurzel
  // (mit einem leeren Knoten).
  BinTree();

  // Gibt den Baum als sortierte Liste aus.
  void print();

  // Fügt ein neues Element in den Baum ein.
  void insert(KEY_TYPE key, VALUE_TYPE value);

  // Sucht das Element mit dem Schlüssel d und gibt dessen Datensatz zurück.
  VALUE_TYPE find(KEY_TYPE key);

  // Entfernt das erste Element mit dem angegebenen Schlüssel.
  void erase(KEY_TYPE key);
};

int main() {
  
  // Leeren Baum erzeugen:
  BinTree<int, int> b1;
  
  b1.insert(42, 1);
  b1.insert(10, 2);
  b1.insert(103, 3);
  b1.insert(200, 4);
  b1.insert(60, 5);
//  b1.insert(75, 6);
  b1.insert(50, 7);
  b1.insert(55, 8);

  b1.print();
  cout << endl;

  b1.erase(60);

  b1.print();

  cout << b1.find(50) << endl;

}

template<typename KEY_TYPE, typename VALUE_TYPE>
bool Element<KEY_TYPE, VALUE_TYPE>::empty()
{
  return links == nullptr && rechts == nullptr;
}

template<typename KEY_TYPE, typename VALUE_TYPE>
void Element<KEY_TYPE, VALUE_TYPE>::print(int depth)
{
  // Pre-Order-Ausgabe mit Einrückungen entsprechend der Tiefe im Baum.
  // --> Ergibt eine lesbare Baumdarstellung (siehe Beispiel in main).
  if (empty()) { return; }

  for (int i=0; i<2*depth; i++) { cout << " "; } //Leissl war hier
  cout << key << " : " << value << endl;
  links->print(depth+1); // Steht für (*links).print();
  rechts->print(depth+1);
}

template<typename KEY_TYPE, typename VALUE_TYPE>
void Element<KEY_TYPE, VALUE_TYPE>::insert(KEY_TYPE key_, VALUE_TYPE value_)
{
  // Wenn der aktuelle Knoten leer ist, neues Element erzeugen.
  if (empty())
  {
    key = key_;
    value = value_;
    links = new Element();
    rechts = new Element();
    return;
  }

  // Knoten ist nicht leer.
  // Je nach Wert von d nach links oder rechts weitergehen.
  if (key_ < key)
  {
    links->insert(key_, value_);
  }
  else
  {
    rechts->insert(key_, value_);
  }
}

template<typename KEY_TYPE, typename VALUE_TYPE>
VALUE_TYPE Element<KEY_TYPE, VALUE_TYPE>::find(KEY_TYPE key_)
{
  Element * result = get_ptr(key_);
  
  // Wenn nichts gefunden wurde, Default-Wert zurückgeben
  if (result == nullptr)
  {
    // TODO: Es bleibt unklar, was im Fehlerfall eine sinnvolle Rückgabe wäre.
    return VALUE_TYPE{};
  }

  return result->value;
}



template<typename KEY_TYPE, typename VALUE_TYPE>
void Element<KEY_TYPE, VALUE_TYPE>::erase(KEY_TYPE key_)
{
  // Ist der aktuelle Knoten leer?
  if (empty())
  {
    return;
  }

  // Den Knoten suchen, der gelöscht werden soll.
  // root ist die Wurzel des Teilbaums, aus dem wir key_ löschen müssen.
  Element * root  = get_ptr(key_);
  
  // Wenn der aktuelle Knoten ein Blatt ist, dann machen wir ihn zum Dummy.
  // D.h. seine Kinder werden zu Nullpointern.
  if (root->links->empty() && root->rechts->empty())
  {
    delete root->links;
    root->links = nullptr;
    delete root->rechts;
    root->rechts = nullptr;
    return;
  }

  // Wenn wir bis hierher kommen, muss der aktuelle Knoten gelöscht werden
  // und er ist kein Blatt.

  Element * next = nullptr;

  // Wenn der rechte Teilbaum existiert...
  if (!root->rechts->empty())
  {
    // Suche das kleinste Element im rechten Teilbaum
    // D.h. das nächstgrößere Element als das, das wir löschen wollen.
    next = root->rechts; // Anfang bei Wurzel des rechten Teilbaums
    // Solange next->links nicht leer ist...
    while (!next->links->empty())
    {
      // ... wandere nach links.
      next = next->links;
    }
  }
  else // Wenn es keinen rechten Teilbaum gibt, muss es einen linken geben.
  {
    // Suche das größte Element im linken Teilbaum.
    next = root->links;
    while (!next->rechts->empty())
    {
      // ... wandere nach links.
      next = next->rechts;
    }
  }

  // Jetzt zeigt next auf den Nachfolger des zu löschenden Elements.
  swap(root->key, next->key);
  swap(root->value, next->value);

  next->erase(key_);


}

template<typename KEY_TYPE, typename VALUE_TYPE>
Element<KEY_TYPE, VALUE_TYPE> * Element<KEY_TYPE, VALUE_TYPE>::get_ptr(KEY_TYPE key_)
{
  // Wenn der aktuelle Knoten leer ist, haben wir nichts gefunden.
  if (empty())
  {
    return nullptr;
  }

  // Wenn die Daten des aktuellen Knotens d entsprechen, haben wir das Element gefunden.
  if (key_ == key) { return this; }

  // Links oder rechts weitersuchen, je nach Wert von d.
  if (key_ < key)
  {
    return links->get_ptr(key_);
  }
  else
  {
    return rechts->get_ptr(key_);
  }
}





template<typename KEY_TYPE, typename VALUE_TYPE>
BinTree<KEY_TYPE, VALUE_TYPE>::BinTree()
: root(new Element<KEY_TYPE, VALUE_TYPE>())
{}

template<typename KEY_TYPE, typename VALUE_TYPE>
void BinTree<KEY_TYPE, VALUE_TYPE>::print()
{
  root->print();
}

template<typename KEY_TYPE, typename VALUE_TYPE>
void BinTree<KEY_TYPE, VALUE_TYPE>::insert(KEY_TYPE key, VALUE_TYPE value)
{
  root->insert(key, value);
}

template<typename KEY_TYPE, typename VALUE_TYPE>
VALUE_TYPE BinTree<KEY_TYPE, VALUE_TYPE>::find(KEY_TYPE d)
{
  return root->find(d);
}

template<typename KEY_TYPE, typename VALUE_TYPE>
void BinTree<KEY_TYPE, VALUE_TYPE>::erase(KEY_TYPE key_)
{
  root->erase(key_);
}
