// Wir definieren eine Element-Datenstruktur
// und eine Klasse für den Binärbaum.
#include <string>
#include <iostream>
using namespace std;

struct Element
{
  string key;
  string value;
  
  Element * links = nullptr;
  Element * rechts = nullptr;

  // Gibt den Teilbaum aus (als sortierte Liste), der zu diesem Element gehört.
  void print(int depth = 0);

  // Fügt ein neues Element in den Teilbaum ein.
  void insert(string key, string value);

  // Sucht das Element mit dem Schlüssel d und gibt dessen Datensatz zurück.
  string find(string key);
};

struct BinTree
{
private:
  Element * root;

public:
  // Konstruktor: Erzeugt einen Baum in initialisiert die Wurzel
  // (mit einem leeren Knoten).
  BinTree();

  // Gibt den Baum als sortierte Liste aus.
  void print();

  // Fügt ein neues Element in den Baum ein.
  void insert(string key, string value);

  // Sucht das Element mit dem Schlüssel d und gibt dessen Datensatz zurück.
  // Anmerkung: In diesem Beispiel sind Schlüssel und Datensatz identisch,
  //            das wird nicht auf Dauer so bleiben.
  string find(string key);
};

int main() {
  
  // Leeren Baum erzeugen:
  BinTree b1;

  b1.insert("Hallo", "Hello");
  b1.insert("Welt", "World");
  b1.insert("Baum", "Tree");

  b1.print();

  cout << b1.find("Baum") << endl; // Soll "Tree" ausgeben.

}

void Element::print(int depth)
{
  // Pre-Order-Ausgabe mit Einrückungen entsprechend der Tiefe im Baum.
  // --> Ergibt eine lesbare Baumdarstellung (siehe Beispiel in main).
  if (links == nullptr && rechts == nullptr) { return; }

  for (int i=0; i<2*depth; i++) { cout << " "; }
  cout << key << " : " << value << endl;
  links->print(depth+1); // Steht für (*links).print();
  rechts->print(depth+1);
}

void Element::insert(string key_, string value_)
{
  // Wenn der aktuelle Knoten leer ist, neues Element erzeugen.
  if (links == nullptr && rechts == nullptr)
  {
    key = key_;
    value = value_;
    links = new Element();
    rechts = new Element();
    return;
  }

  // Knoten ist nicht leer.
  // Je nach Wert von d nach links oder rechts weitergehen.
  if (key_ < key)
  {
    links->insert(key_, value_);
  }
  else
  {
    rechts->insert(key_, value_);
  }
}

string Element::find(string key_)
{
  // Wenn der aktuelle Knoten leer ist, haben wir nichts gefunden.
  if (links == nullptr && rechts == nullptr)
  {
    return "";
  }

  // Wenn die Daten des aktuellen Knotens d entsprechen, haben das Element gefunden.
  if (key_ == key) { return value; }

  // Links oder rechts weitersuchen, je nach Wert von d.
  if (key_ < key)
  {
    return links->find(key_);
  }
  else
  {
    return rechts->find(key_);
  }
}

BinTree::BinTree()
: root(new Element())
{}

void BinTree::print()
{
  root->print();
}

void BinTree::insert(string key, string value)
{
  root->insert(key, value);
}

string BinTree::find(string d)
{
  return root->find(d);
}

