

// Wir definieren eine Element-Datenstruktur
// und eine Klasse für den Binärbaum.
#include <string>
#include <iostream>
using namespace std;

template<typename KEY_TYPE, typename VALUE_TYPE>
struct Element
{
  KEY_TYPE key;
  VALUE_TYPE value;
  
  Element * links = nullptr;
  Element * rechts = nullptr;
  
  int height = 0;

  // Liefert true zurück, wenn das Element ein leerer Baum ist (also ein Dummy).
  bool empty();

  // Gibt den Teilbaum aus (als sortierte Liste), der zu diesem Element gehört.
  void print(int depth = 0);

  // Fügt ein neues Element in den Teilbaum ein.
  void insert(KEY_TYPE key, VALUE_TYPE value);

  // Sucht das Element mit dem Schlüssel d und gibt dessen Datensatz zurück.
  VALUE_TYPE find(KEY_TYPE key);

  // Entfernt das erste Element mit dem angegebenen Schlüssel.
  void erase(KEY_TYPE key);

  // Liefert einen Pointer auf das Element mit dem angegeben Schlüssel.
  Element * get_ptr(KEY_TYPE key);

  // Rotationen
  void rotate_left();
  void rotate_right();
  void rotate_leftRight();
  void rotate_rightLeft();

};

template<typename KEY_TYPE, typename VALUE_TYPE>
struct BinTree
{
private:
  Element<KEY_TYPE, VALUE_TYPE> * root;

public:
  // Konstruktor: Erzeugt einen Baum und initialisiert die Wurzel
  // (mit einem leeren Knoten).
  BinTree();

  // Gibt den Baum als sortierte Liste aus.
  void print();

  // Fügt ein neues Element in den Baum ein.
  void insert(KEY_TYPE key, VALUE_TYPE value);

  // Sucht das Element mit dem Schlüssel d und gibt dessen Datensatz zurück.
  VALUE_TYPE find(KEY_TYPE key);

  // Entfernt das erste Element mit dem angegebenen Schlüssel.
  void erase(KEY_TYPE key);

};

int main() {
  
  // Leeren Baum erzeugen:
  BinTree<int, int> b1;
  
  b1.insert(42, 1);
  b1.insert(10, 2);
  b1.insert(103, 3);
  b1.insert(200, 4);
  b1.insert(60, 5);
  b1.insert(300, 6);
//  b1.insert(50, 7);
//  b1.insert(55, 8);

  b1.print();
}

template<typename KEY_TYPE, typename VALUE_TYPE>
bool Element<KEY_TYPE, VALUE_TYPE>::empty()
{
  return links == nullptr && rechts == nullptr;
}

template<typename KEY_TYPE, typename VALUE_TYPE>
void Element<KEY_TYPE, VALUE_TYPE>::print(int depth)
{
  // Pre-Order-Ausgabe mit Einrückungen entsprechend der Tiefe im Baum.
  // --> Ergibt eine lesbare Baumdarstellung (siehe Beispiel in main).
  if (empty()) { return; }

  for (int i=0; i<2*depth; i++) { cout << " "; } //Leissl war hier
  cout << key << " : " << value << endl;
  links->print(depth+1); // Steht für (*links).print();
  rechts->print(depth+1);
}

// Aufgabe 2: Erweitern Sie die insert()-Funktion, so dass sie die Höhe des Teilbaums pflegt,
// auf dem sie arbeitet.
template<typename KEY_TYPE, typename VALUE_TYPE>
void Element<KEY_TYPE, VALUE_TYPE>::insert(KEY_TYPE key_, VALUE_TYPE value_)
{
  // Wenn der aktuelle Knoten leer ist, neues Element erzeugen.
  if (empty())
  {
    key = key_;
    value = value_;
    height = 1;
    links = new Element();
    rechts = new Element();
    return;
  }

  // Knoten ist nicht leer.
  // Je nach Wert von d nach links oder rechts weitergehen.
  if (key_ < key)
  {
    links->insert(key_, value_);
  }
  else
  {
    rechts->insert(key_, value_);
  }

  // Nachdem die Rekursion zurückgekehrt ist, könnte die Höhe eines Teilbaums geändert haben
  // und damit muss ggf. die eigene Höhe angepasst werden.
  // Wir können (wegen der Rekursion) davon ausgehen, dass die Höheneinträge der Kinder stimmen.
  if (links->height > rechts->height)
  {
    height = links->height + 1;
  }
  else
  {
    height = rechts->height + 1;
  }
  
  // AUFGABE: Hier die Rotationen wirklich durchführen
  // 1. Prüfen, ob und welche Rotation notwendig ist.
  //    - Dafür Balancefaktoren berechnen.
  // 2. Die korrekte Rotation durchführen

  int bf = rechts->height - links->height;
  if (bf == -1 || bf == 0 || bf == 1)
  {
    // In diesem Fall ist nichts zu tun, weil kein
    // Ungleichgewicht besteht.
    return;
  }
  
  // Wenn wir hier ankommen, muss eines der Kinder geprüft werden.
  if (bf <= -2)
  {
    int bf_links = links->rechts->height - links->links->height;
    if (bf_links < 0) {
      rotate_right();
    }
    else
    {
      rotate_leftRight();
    }
  }
  if (bf >= 2)
  {
    int bf_rechts = rechts->rechts->height - rechts->links->height;
    if (bf_rechts > 0) {
      rotate_left();
    }
    else
    {
      rotate_rightLeft();
    }
  }

  // Schreiben Sie hier Code, der entscheidet, ob Rotationen notwendig sind und geben Sie die Entscheidung per cout bekannt.

}

template<typename KEY_TYPE, typename VALUE_TYPE>
VALUE_TYPE Element<KEY_TYPE, VALUE_TYPE>::find(KEY_TYPE key_)
{
  Element * result = get_ptr(key_);
  
  // Wenn nichts gefunden wurde, Default-Wert zurückgeben
  if (result == nullptr)
  {
    // TODO: Es bleibt unklar, was im Fehlerfall eine sinnvolle Rückgabe wäre.
    return VALUE_TYPE{};
  }

  return result->value;
}


// Aufgabe 3: Erweitern Sie die erase()-Funktion, so dass sie die Höhe pflegt.
template<typename KEY_TYPE, typename VALUE_TYPE>
void Element<KEY_TYPE, VALUE_TYPE>::erase(KEY_TYPE key_)
{
  // Ist der aktuelle Knoten leer?
  if (empty())
  {
    return;
  }

  // Falls der aktuelle Knoten nicht der ist, der gelöscht werden soll,
  // absteigen, bis er gefunden ist.
  if (key_ < key)
  {
    links->erase(key_);
    return;
  }
  else
  {
    rechts->erase(key_);
    return;
  }
  
  
  // Wenn der aktuelle Knoten ein Blatt ist, dann machen wir ihn zum Dummy.
  // D.h. seine Kinder werden zu Nullpointern.
  if (links->empty() && rechts->empty())
  {
    delete links;
    links = nullptr;
    delete rechts;
    rechts = nullptr;
    return;
  }

  // Wenn wir bis hierher kommen, muss der aktuelle Knoten gelöscht werden
  // und er ist kein Blatt.

  Element * next = nullptr;

  // Wenn der rechte Teilbaum existiert...
  if (!rechts->empty())
  {
    // Suche das kleinste Element im rechten Teilbaum
    // D.h. das nächstgrößere Element als das, das wir löschen wollen.
    next = rechts; // Anfang bei Wurzel des rechten Teilbaums
    // Solange next->links nicht leer ist...
    while (!next->links->empty())
    {
      // ... wandere nach links.
      next = next->links;
    }
  }
  else // Wenn es keinen rechten Teilbaum gibt, muss es einen linken geben.
  {
    // Suche das größte Element im linken Teilbaum.
    next = links;
    while (!next->rechts->empty())
    {
      // ... wandere nach links.
      next = next->rechts;
    }
  }

  // Jetzt zeigt next auf den Nachfolger des zu löschenden Elements.
  swap(key, next->key);
  swap(value, next->value);

  next->erase(key_);


}

template<typename KEY_TYPE, typename VALUE_TYPE>
Element<KEY_TYPE, VALUE_TYPE> * Element<KEY_TYPE, VALUE_TYPE>::get_ptr(KEY_TYPE key_)
{
  // Wenn der aktuelle Knoten leer ist, haben wir nichts gefunden.
  if (empty())
  {
    return nullptr;
  }

  // Wenn die Daten des aktuellen Knotens d entsprechen, haben wir das Element gefunden.
  if (key_ == key) { return this; }

  // Links oder rechts weitersuchen, je nach Wert von d.
  if (key_ < key)
  {
    return links->get_ptr(key_);
  }
  else
  {
    return rechts->get_ptr(key_);
  }
}

template<typename KEY_TYPE, typename VALUE_TYPE>
void Element<KEY_TYPE, VALUE_TYPE>::rotate_left()
{
  cout << "rotate_left()" << endl;

  Element * A_alt = this;
  Element * B = links;
  Element * C = rechts;
  Element * D = C->links;
  Element * E = C->rechts;

  // Neuen Knoten A_neu zwischen A_alt und B erzeugen...
  Element * A_neu = new Element();
  A_neu->links = B;
  A_alt->links = A_neu;

  // ... und Werte von A_alt kopieren.
  A_neu->key = A_alt->key;
  A_neu->value = A_alt->value;

  // Werte C nach A_alt kopieren
  A_alt->key = C->key;
  A_alt->value = C->value;

  // D wird rechts an A_neu angehängt.
  A_neu->rechts = D;

  // E wird rechts an die Wurzel (A_alt) angehängt.
  A_alt->rechts = E;

  // Alten Knoten C löschen
  delete C;


  // TODO: Höhen der Knoten aktualisieren.
  
  
  // Spezialfälle:
  // 1. Wurzel leer:
  //   - Dann ist nichts zu tun.
  //   - Entweder wir verlassen uns darauf, dass
  //     rotate_left() dann gar nicht aufgerufen wird,
  //     oder wir gehen auf Nummer sicher und machen
  //     dann nichts.
  // TODO
}

template<typename KEY_TYPE, typename VALUE_TYPE>
void Element<KEY_TYPE, VALUE_TYPE>::rotate_right()
{
  cout << "rotate_right()" << endl;
  Element * A_alt = this;
  Element * B = links;
  Element * C = rechts;
  Element * D = B->links;
  Element * E = B->rechts;

  // Neuen Knoten A_neu zwischen A und C erzeugen
  // und Werte von A kopieren.

  // Werte B nach A kopieren

  // E links an A2 anhängen.
  // D links an A anhängen, B löschen.

}

template<typename KEY_TYPE, typename VALUE_TYPE>
void Element<KEY_TYPE, VALUE_TYPE>::rotate_leftRight()
{
    cout << "rotate_leftRight()" << endl;
}

template<typename KEY_TYPE, typename VALUE_TYPE>
void Element<KEY_TYPE, VALUE_TYPE>::rotate_rightLeft()
{
    cout << "rotate_rightLeft()" << endl;
}




template<typename KEY_TYPE, typename VALUE_TYPE>
BinTree<KEY_TYPE, VALUE_TYPE>::BinTree()
: root(new Element<KEY_TYPE, VALUE_TYPE>())
{}

template<typename KEY_TYPE, typename VALUE_TYPE>
void BinTree<KEY_TYPE, VALUE_TYPE>::print()
{
  root->print();
}

template<typename KEY_TYPE, typename VALUE_TYPE>
void BinTree<KEY_TYPE, VALUE_TYPE>::insert(KEY_TYPE key, VALUE_TYPE value)
{
  root->insert(key, value);
}

template<typename KEY_TYPE, typename VALUE_TYPE>
VALUE_TYPE BinTree<KEY_TYPE, VALUE_TYPE>::find(KEY_TYPE d)
{
  return root->find(d);
}

template<typename KEY_TYPE, typename VALUE_TYPE>
void BinTree<KEY_TYPE, VALUE_TYPE>::erase(KEY_TYPE key_)
{
  root->erase(key_);
}
